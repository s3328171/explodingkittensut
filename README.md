Important things to know about our project:

1. Two of the test methods need the Mockito library from Maven, which maybe needs to be added to your IntelliJ
2. The test methods test the offline version of the game. It is the same game without the server connection. This was done so we did not have to simulate a server connection for the test cases.
3. To start the server connection, first run the GameServer class, to start the clients, run the GameClient class.
4. The first (correct) input of the game of the first player is not read, it has to be entered twice. After that, it is not an issue.
