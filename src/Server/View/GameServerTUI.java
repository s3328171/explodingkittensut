package Server.View;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class GameServerTUI {

    /** The PrintWriter to write messages to */
    private PrintWriter console;
    private BufferedReader reader;

    /**
     * Constructs a new HotelServerTUI. Initializes the console.
     */
    public GameServerTUI() {
        console = new PrintWriter(System.out, true);
        reader = new BufferedReader(new InputStreamReader(System.in));
    }


    public void showMessage(String message) {
        console.println(message);
    }


    public String getString(String question) {
        console.print(question + " ");
        console.flush();
        try {
            return reader.readLine();
        } catch (IOException e) {
            showMessage("Error reading the input.");
            return null;
        }
    }


    public int getInt(String question) {
        while (true) {
            try {
                String input = getString(question);
                return Integer.parseInt(input);
            } catch (NumberFormatException e) {
                showMessage("Invalid input. Please enter a valid integer.");
            }
        }
    }


    public boolean getBoolean(String question) {
        while (true) {
            try {
                console.print(question + " (yes/no) ");
                String input = reader.readLine().toLowerCase();
                if (input.equals("yes")) {
                    return true;
                } else if (input.equals("no")) {
                    return false;
                } else {
                    showMessage("Invalid input. Please enter 'yes' or 'no'.");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
