package Server.View;

import Server.Control.GameController;
import Server.Model.Card;
import Server.Model.ComputerPlayer;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class GameView {
    private GameController gameController;

    public GameView(GameController gameController){
        this.gameController = gameController;
    }
    public void displayWelcomeMessage() {
        System.out.println("Welcome to Exploding Kittens!");
    }

    // sends the opposing player their hand, and asks them to pick one to give to the current player.
    public int favourCard() {
        while(true) {
            gameController.getServer().sendToOpposingPlayer(displayPlayerHand(gameController.getOpposingPlayer().getName(), gameController.getOpposingPlayer().getHand()));
            try {
                int positionInHand = gameController.getServer().favourCard(gameController.getOpposingPlayer().toString() + ", choose a card to give to " + gameController.getCurrentPlayer().toString() + ": (FC + the number of the card)");

                // if the answer received equals -99, something went wrong with the connection, and the other player is informed about this.
                if(positionInHand == -99){
                    gameController.setGameRunning(false);
                    gameController.getServer().broadcastMessage("The other player disconnected, you won!");
                    return positionInHand;
                }

                // also checks that the answer refers to a valid position in the players hand, and if this is not the case, prompts the player to try again
                else if (positionInHand >= 1 && positionInHand <= gameController.getOpposingPlayer().getHand().size()) {
                    return positionInHand;
                } else {
                    gameController.getServer().sendToOpposingPlayer("Oops, please only enter valid numbers between 1 and " + gameController.getOpposingPlayer().getHand().size());
                }
            }
            catch (NumberFormatException e){
                gameController.getServer().sendToOpposingPlayer("Oops, please only enter valid numbers between 1 and " + gameController.getOpposingPlayer().getHand().size());
            }
        }
    }


    public String displayPlayerHand(String playerName, List<Card> hand) {
        StringBuilder result = new StringBuilder(playerName + "'s hand: \n");
        for (int i = 0; i < hand.size(); i++) {
            result.append(i + 1).append(": ").append(hand.get(i).toString()).append("\n");
        }
        return result.toString();
    }


    public String displayTurnOptions() {
        StringBuilder result = new StringBuilder();
        result.append("PC 0: Draw a card\n");
        result.append("PC 1 - ...: Playing a card from your hand. 1 being the most left one.\n");
        result.append("PC -2: playing a combo of 2 cards\n");
        result.append("PC -3: playing a combo of three cards\n");
        return result.toString();
    }


    public String defuseCardEmptyDeck(){
        return "You drew a Exploding kitten! Luckily you have a defuse card to defuse the bomb. \n" +
                "Oh the deck is empty! Just put it back on top of the deck!";
    }


    //asks the opposing player if they want to continue shuffle
    public boolean shouldContinueShuffle() {
        while (true) {

            String input = gameController.getServer().shuffleCard("Do you want to continue shuffling? (BQ yes/BQ no)");
            if(input.equals("ERROR")){
                gameController.setGameRunning(false);
                gameController.getServer().broadcastMessage("The other player disconnected, you won!");
                return false;
            }
            // Check if the input is a valid response
            else if (input.equalsIgnoreCase("yes")) {
                return true;
            } else if (input.equalsIgnoreCase("no")) {
                return false;
            } else {
                // Invalid input, prompt the user again
                System.out.println("Invalid input. Please enter 'BQ yes' or 'BQ no'.");
            }
        }
    }


    public void computerPlayerTurn(Card card){
        gameController.getServer().broadcastMessage("The computer player played: " + card);
    }


    public String playerTurn (Card card){
        return "The player " + gameController.getCurrentPlayer().getName() + " played: "+ card;
    }



    public int defuseCard(){
        int result;
        //if the current player is a computer player, it is chosen at random where the exploding kitten is being put
        if (gameController.getCurrentPlayer() instanceof ComputerPlayer){
            gameController.getServer().broadcastMessage("The ComputerPlayer drew a Exploding kitten! It had a defuse card to defuse the bomb.");
            Random random = new Random();
            int limit = gameController.getDeck().getDeck().size()-1;
            result = random.nextInt(limit);
            return result;
        }
        else {
            //informs the player that they drew an exploding kitten and that they can defuse it, and asks them where they want to put it in the deck
            while (true) {
                try {
                    gameController.getServer().sendToPlayer("You drew a Exploding kitten! Luckily you have a defuse card to defuse the bomb.");
                    result = gameController.getServer().askForCard("Where do you want to put the kitten in the deck? PC 1 being on top of the deck, and PC " + gameController.getDeck().getDeck().size() + " being the bottom.");
                    //if the answer is -99 something went wrong with the server connection
                    if(result == -99){
                        gameController.setGameRunning(false);
                        gameController.getServer().broadcastMessage("The other player disconnected, you won!");
                        break;
                    }
                    //checks if the answer is within the bounds of the remaining deck
                    else if (result < 1 || result > gameController.getDeck().getDeck().size()) {
                        gameController.getServer().sendToPlayer("Invalid input. Please enter a number between 1 and " + gameController.getDeck().getDeck().size());
                    } else {//if the answer is valid, the loop breaks anf the answer is returned
                        break;
                    }
                } catch (InputMismatchException e) {
                    gameController.getServer().sendToPlayer("Invalid input. Please enter a number between 1 and " + gameController.getDeck().getDeck().size());

                }
            }
        }

        return result;
    }


    public String drewAndEnded(){
        return gameController.getCurrentPlayer().getName() + " drew a card and ended their turn!";
    }


    public String printCurrentPlayer(){
         return "It is " + gameController.getCurrentPlayer() + "'s turn!";
    }


    public String nopeHasBeenFound(){
        String answer;
        //if the current player is a Computer player, the answer is always no
        if(gameController.getOpposingPlayer() instanceof ComputerPlayer){
            gameController.getServer().broadcastMessage("The ComputerPlayer has a nope card!");
            gameController.getServer().broadcastMessage("It said no!");
            return "no";
        }
        else {
            //do-while loop runs until a valid answer is received
            do {
                answer = gameController.getServer().nopeCard(gameController.getOpposingPlayer().toString() + ", you have a Nope card on your hand! Do you want to play it and stop this action? (BQ yes/BQ no)");
                if(answer.equals("ERROR")){
                    gameController.setGameRunning(false);
                    gameController.getServer().broadcastMessage("The other player disconnected, you won!");
                    return "no";
                }
                if (!answer.equals("yes") && !answer.equals("no")) {
                    gameController.getServer().sendToOpposingPlayer("Invalid input. Please enter 'BQ yes' or 'BQ no'.");
                }
            } while (!answer.equals("yes") && !answer.equals("no"));
        }
        return answer.toLowerCase();
    }

    public void noped(){
        gameController.getServer().broadcastMessage("NOPE");
    }

    //same as the nopeHasBeenFound method but for the current player
    public String yupHasBeenFound(){
        String answer;
        if(gameController.getCurrentPlayer() instanceof ComputerPlayer){
            gameController.getServer().broadcastMessage("The ComputerPlayer also has a nope card!");
            gameController.getServer().broadcastMessage("It said no!");
            return "no";
        }
        do {
            answer = gameController.getServer().yupCard(gameController.getCurrentPlayer().toString() + ", you have a Nope card on your hand! Do you want to play it and stop this action? (BQ yes/BQ no)");
            if(answer.equals("ERROR")){
                gameController.setGameRunning(false);
                gameController.getServer().broadcastMessage("The other player disconnected, you won!");
                return "no";
            }
            else if (!answer.equals("yes") && !answer.equals("no")) {
                gameController.getServer().sendToPlayer("Invalid input. Please enter 'BQ yes' or 'BQ no'.");
            }
        }while(!answer.equals("yes") && !answer.equals("no"));

        return answer.toLowerCase();
    }

    public void yup(){
        gameController.getServer().broadcastMessage("YUP");
    }


    public void invalidCard(){
        gameController.getServer().sendToPlayer("Oops that is not a valid card to play! Try again!");
    }


    public String attackCardMessage(){
        return gameController.getCurrentPlayer().toString() + ", you have been attacked! it is now your turn \n" +
                "and you have to go " + gameController.getCurrentPlayer().getRemainingTurns() + " times!";
    }


    public int getPlayerChoice() {
        int answer;
        //loops until a valid answer is received
        while (true) {
            try {
                answer = gameController.getServer().askForCard("Choose an option: ");
                //if the answer is -99, something went wrong with the server
                if(answer == -99){
                    gameController.setGameRunning(false);
                    gameController.getServer().broadcastMessage("The other player disconnected, you won!");
                    break;
                }
                else if (answer  >=-3 && answer <= gameController.getCurrentPlayer().getHand().size()) {
                    break; // Valid input, exit the loop
                }
                gameController.getServer().sendToPlayer("Invalid input. Please enter PC and a number between -3(to play a three card combo), " +
                        "-2 (to play a two card combo), 0 (to draw) and " + (gameController.getCurrentPlayer().getHand().size()) + "(to play that car from your hand)");
            } catch (InputMismatchException|NumberFormatException e) {
                gameController.getServer().sendToPlayer("Invalid input. Please enter PC and a number between -3(to play a three card combo), " +
                        "-2 (to play a two card combo), 0 (to draw) and " + (gameController.getCurrentPlayer().getHand().size()) + "(to play that car from your hand)");;
            }
        }
        return answer;
    }


    //same as the getPlayerChoice method but used when the player wants to play a combo
    public int getPlayerComboChoice(int i){
        int answer;
        while(true) {
            try {
                answer = gameController.getServer().askForCard("Choose " + i + " combo card: (PC [Position in your hand])");
                if(answer == -99){
                    gameController.setGameRunning(false);
                    gameController.getServer().broadcastMessage("The other player disconnected, you won!");
                    break;
                }
                else if (answer < 0 || answer > gameController.getCurrentPlayer().getHand().size()) {
                    gameController.getServer().sendToPlayer("Invalid input. Please enter a number between 0 (to draw) and " + (gameController.getCurrentPlayer().getHand().size()) + "(to play that car from your hand)");;
                }
                else{
                    break;
                }
            } catch (InputMismatchException e) {
                gameController.getServer().sendToPlayer("Invalid input. Please enter a number between 0 (to draw) and " + (gameController.getCurrentPlayer().getHand().size()) + "(to play that car from your hand)");;

            }
        }
        return answer;
    }

    public String seeFutureCard(){
        return gameController.getDeck().getDeck().get(0) + "\n" +
                gameController.getDeck().getDeck().get(1) + "\n" +
                gameController.getDeck().getDeck().get(2) + "\n";
    }


    public void displayExplodedMessage() {
        gameController.getServer().sendToPlayer("BOOM! You drew an Exploding Kitten! Game Over!");
        gameController.getServer().sendToOpposingPlayer("The other player exploded! Congratulations, you won!");
        gameController.getServer().broadcastMessage("Game Over");
    }
}
