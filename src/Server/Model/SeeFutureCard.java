package Server.Model;

import Server.Control.GameController;

public class SeeFutureCard extends Card {
    Deck deck;
    GameController gameController;
    public SeeFutureCard(GameController gameController, Deck deck){
        this.gameController = gameController;
        this.deck = deck;
    }



    @Override
    public void activate() {
        viewTopThreeCards();
    }
    @Override
    public String toString(){
        return getClass().getSimpleName();
    }



    public void viewTopThreeCards(){
        System.out.println(deck.getDeck().get(0));
        System.out.println(deck.getDeck().get(1));
        System.out.println(deck.getDeck().get(2));
    }
}
