package Server.Model;

public class CatCards extends Card {

    private String name;

    public CatCards(String name) {

        this.name = name;
    }

    @Override
    public String toString(){

        return name;
    }

    @Override
    public void activate(){

    }

    public boolean canCombineWith(CatCards otherCard){
        //Checks if two CatCards have the same name and thus can used together
        return this.name.equalsIgnoreCase(otherCard.name);
    }

}
