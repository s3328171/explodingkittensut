package Server.Model;

public class ExplodingKitten extends Card {
    public ExplodingKitten(){}
    @Override
    public String toString(){
        return getClass().getSimpleName();
    }
    public void explode(){};
}
