package Server.Model;

import Server.Control.GameController;
import Server.View.GameView;

public class NopeCard extends Card {
    private Deck deck;
    private GameController gameController;
    private GameView view;
    private boolean keepGoing = true;
    private boolean noped = false;
    public NopeCard(Deck deck, GameController gameController, GameView view){
        this.deck = deck;
        this.gameController = gameController;
        this.view = view;
    }

    @Override
    public void activate() {
        nope();
    }
    @Override
    public String toString(){
        return getClass().getSimpleName();
    }

    //checks if the opposing player has a nopeCard, and asks if it should be played. if yes, the variable "nope" in the controller is toggled
    //if the answer is no, the local variable keepGoing is set to false
    public void nope(){
        System.out.println("test nope method nopeCart");
        if(gameController.getOpposingPlayer().getHand().stream().anyMatch(card -> card instanceof NopeCard)){
            String answer = view.nopeHasBeenFound();
            if(answer.equals("yes")){
                gameController.toggleNoped();
                for(Card card : gameController.getOpposingPlayer().getHand()){
                    if(card instanceof NopeCard){
                        gameController.getOpposingPlayer().getHand().remove(card);
                        break;
                    }
                }
                view.noped();
                noped = true;
            }
            else{
                keepGoing = false;
            }
        }
    }

    //same as the nope() method, but for the currentplayer
    public void yup(){
        System.out.println("test yup method nopCart");
        if(gameController.getCurrentPlayer().getHand().stream().anyMatch(card -> card instanceof NopeCard) && noped){
            String answer = view.yupHasBeenFound();
            if(answer.equals("yes")){
                gameController.toggleNoped();
                for(Card card : gameController.getCurrentPlayer().getHand()){
                    if(card instanceof NopeCard){
                        gameController.getCurrentPlayer().getHand().remove(card);
                        break;
                    }
                }
                view.yup();
            }
            else{
                keepGoing = false;
            }
        }
        else{
            keepGoing = false;
        }

    }

    //handles the cycle of nopes and yups if both players have nopeCards. if one of them doesn't have one or said no, the loop terminates
    public void nopeYupCycle(){
        keepGoing = true;
        while(keepGoing){
            System.out.println("under keepgoing in nopeYup");
            if(gameController.getOpposingPlayer().getHand().stream().anyMatch(card -> card instanceof NopeCard)){
                nope();
                if(gameController.getCurrentPlayer().getHand().stream().anyMatch(card -> card instanceof NopeCard)){
                    yup();
                }
                else {
                    break;
                }
            }
            else{
                break;
            }

        }

    }

}
