package Server.Model;

public abstract class Card {
    private Player owner;

    public Card() {
        this.owner = null;
    }

    public Player getOwner() {
        return owner;
    }

    public void setOwner(Player owner) {
        this.owner = owner;
    }

    public void activate(){}

}

