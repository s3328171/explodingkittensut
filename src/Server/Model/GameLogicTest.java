package Server.Model;

import Server.Control.GameController;
import Server.View.GameView;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class GameLogicTest extends GameController {
    private GameController gameController;
    private Deck testDeck;
    private GameView gameView;

    public GameLogicTest(){

    }

    @BeforeEach
    public void setUp(){
        gameController = new GameController();

    }

    @Test
    public void testPlayers(){

    }

    @Test
    public void testAttackCard(){
        gameController.getCurrentPlayer().addToHand(new AttackCard(gameController, testDeck, gameView));
        assertEquals("blank" ,gameController.getCurrentPlayer().getName());
        gameController.getCurrentPlayer().play(gameController.getCurrentPlayer().getHand().size(), testDeck);
        gameController.activateCard();
        assertEquals("blank2" ,gameController.getCurrentPlayer().getName());
        assertEquals(3, gameController.getCurrentPlayer().getRemainingTurns());
    }

    @Test
    public void testFavorCard(){
        gameController.getCurrentPlayer().addToHand(new FavorCard(gameController));
        int handsizeCurrent = gameController.getCurrentPlayer().getHand().size();
        int handsizeOpposing = gameController.getOpposingPlayer().getHand().size();
        gameController.getCurrentPlayer().play(gameController.getCurrentPlayer().getHand().size(), testDeck);
        gameController.activateCard();
        assertEquals(handsizeCurrent+1, gameController.getCurrentPlayer().getHand().size());
        assertEquals(handsizeOpposing+1, gameController.getOpposingPlayer().getHand().size());
    }

    @Test
    public void testShuffleCard(){
        gameController.getCurrentPlayer().addToHand(new ShuffleCard(gameController));
        gameController.getCurrentPlayer().play(gameController.getCurrentPlayer().getHand().size(), testDeck);
        //how do i test a shuffle?
    }

    @Test
    public void basicTest(){
        assertNotEquals(0, gameController.getCurrentPlayer().getHand().size());
    }

    //test for favour card, defuse card, skip card, seeFutureCard are missing

}
