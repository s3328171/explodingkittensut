package Server.Model;



import Server.Control.GameController;

import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class SpecialCombos extends Card {

    private String name;
    private static GameController gameController;

    public SpecialCombos(String name, GameController gameController){
        this.gameController = gameController;
        this.name = name;
    }

    @Override
    public String toString(){
        return name;
    }


    //Cards need to be from the same suit in order to be considered eligible for canPlayAsPair
    public static boolean canPlayAsPair(List<Card> comboCards) {
        // Check if there is a pair of cards with the same kind (suit) in the hand
        for (int i = 0; i < comboCards.size(); i++) {
            for (int j = i + 1; j < comboCards.size(); j++) {
                if (comboCards.get(i).getClass().equals(comboCards.get(j).getClass()) &&
                        areCardsFromSameKind(comboCards.get(i), comboCards.get(j))) {
                    return true;
                }
            }
        }
        return false;
    }


    public static boolean canPlayAsTriplet(List<Card> comboCards) {
        // Check if there is a triplet of cards with the same kind (suit) in the hand
        for (int i = 0; i < comboCards.size(); i++) {
            for (int j = i + 1; j < comboCards.size(); j++) {
                for (int k = j + 1; k < comboCards.size(); k++) {
                    if (comboCards.get(i).getClass().equals(comboCards.get(j).getClass()) &&
                            comboCards.get(i).getClass().equals(comboCards.get(k).getClass()) &&
                            areCardsFromSameKind(comboCards.get(i), comboCards.get(j)) &&
                            areCardsFromSameKind(comboCards.get(i), comboCards.get(k))) {
                        return true;
                    }
                }
            }
        }
        return false;
    }


    private static boolean areCardsFromSameKind(Card card1, Card card2) {
        if (card1 instanceof CatCards && card2 instanceof CatCards) {
            //If both cards are CatCards, this checks if they are of the same CatCard type
            CatCards catCard1 = (CatCards) card1;
            CatCards catCard2 = (CatCards) card2;
            return catCard1.canCombineWith(catCard2);
        } else {
            return card1.getClass().equals(card2.getClass());
        }
    }


    public static void stealRandomCard(Player currentPlayer, Player targetPlayer) {
        // Pick a random card from the target player's hand and give it to the current player
        List<Card> targetHand = targetPlayer.getHand();
        if (!targetHand.isEmpty()) {
            Random random = new Random();
            int randomIndex = random.nextInt(targetHand.size());
            Card stolenCard = targetHand.remove(randomIndex);
            currentPlayer.addToHand(stolenCard);
            gameController.getServer().broadcastMessage(currentPlayer.getName() + " stole a card from " + targetPlayer.getName());
        } else {
            gameController.getServer().broadcastMessage(targetPlayer.getName() + "'s hand is empty. No card stolen.");
        }
    }


    public static void nameAndCheckCard(Player currentPlayer, Player targetPlayer, String cardName) {
        // Check if the target player has the named card, and if so, remove it
        List<Card> targetHand = targetPlayer.getHand();
        boolean cardFound = false;

        for (Card card : targetHand) {
            if(card.toString().equals(cardName)){
                targetHand.remove(card);
                currentPlayer.getHand().add(card);
                cardFound = true;
                break;
            }
        }

        if (cardFound) {
            gameController.getServer().broadcastMessage(targetPlayer.getName() + " had the named card. " + currentPlayer.getName() + " successfully named and stole it.");
        } else {
            gameController.getServer().broadcastMessage(targetPlayer.getName() + " did not have the named card. " + currentPlayer.getName() + " is out of luck.");
        }
    }


    public static void playPair(Player currentPlayer, Player targetPlayer, Card[] comboCards) {
        // Player decides to play a pair

        if (canPlayAsPair(List.of(comboCards))) {
            stealRandomCard(currentPlayer, targetPlayer);
            gameController.setValidSpecialComboPlay(true);
        }
        else{
            gameController.getServer().sendToPlayer("The cards have to be of the same suit!");
            gameController.setValidSpecialComboPlay(false);
        }
    }


    public static void playTriplet(Player currentPlayer, Player targetPlayer, Card[] comboCards) {
        List<Card> currentPlayerHand = currentPlayer.getHand();

        // Player decided to play a triplet
        if (canPlayAsTriplet(List.of(comboCards))) {
            String cardName = gameController.getServer().specialCombosThree("Enter the name of the card you want: (SC [name of card as it is displayed])");
            if(cardName.equals("ERROR")){
                gameController.setGameRunning(false);
            }
            else {
                nameAndCheckCard(currentPlayer, targetPlayer, cardName);
                gameController.setValidSpecialComboPlay(true);
            }
        }
        else{
            gameController.getServer().sendToPlayer("The cards have to be of the same suit!");
            gameController.setValidSpecialComboPlay(false);
        }
    }


}