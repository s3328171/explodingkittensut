package Server.Model;

import Server.Control.GameController;
import Server.View.GameView;

public class AttackCard extends Card {
    GameController gameController;
    Deck deck;
    GameView view;

    public AttackCard(GameController gameController, Deck deck, GameView view){
        this.gameController = gameController;
        this.deck = deck;
        this.view = view;
    }

    /**
     * sets the variables playerTurn and draw of the gamecontroller to false. This ensures that the turn of the player playing the attack card
     * ends without them drawing a card. Then it checks the attack flag of the gamecontroller. If the attack flag is true, then the opposing player gets
     * 2 turns + the remaining turns of the current player set as their remaining turns and the current player's turns get reset.
     * If the attack flag is false, the opposing player's turns get set to 2 and the current players turns get reset.
     */
    @Override
    public void activate(){
        gameController.setPlayerTurn(false);
        gameController.setDraw(false);
        if(gameController.isAttackFlag()) {
            gameController.getOpposingPlayer().setRemainingTurns(2 + gameController.getCurrentPlayer().getRemainingTurns());
            gameController.getCurrentPlayer().setRemainingTurns(0);
        }
        else{
            gameController.getOpposingPlayer().setRemainingTurns(2);
            gameController.getCurrentPlayer().setRemainingTurns(0);
        }
    }

    @Override
    public String toString(){
        return getClass().getSimpleName();
    }


}


