package Server.Model;



import Server.Control.GameController;

import java.util.ArrayList;
import java.util.List;

public class Player {
    private GameController gameController;
    private List<Card> hand;
    private String name;
    private int remainingTurns;


    public Player(String name, GameController gameController){
        this.name = name;
        hand = new ArrayList<>();
        remainingTurns = 1;
        this.gameController = gameController;
    }
    @Override
    public String toString(){
        return name;
    }


    public void play(int card, Deck playingDeck){
        if(hand.isEmpty()){
            System.out.println("Your hand is empty");
        }
        else{
            Card playedCard = getHand().get(card-1);
            playingDeck.addToActivePile(playedCard);
            //After turn, decrements the remaining turns
            removeFromHand(playedCard);
        }

    }


    public void addTurns(int turns) {
        remainingTurns += turns;
    }

    public int getRemainingTurns() {
        return remainingTurns;
    }

    public void resetRemainingTurns() {
        remainingTurns = 1;
    }

    public void setRemainingTurns(int remainingTurns) {
        this.remainingTurns = remainingTurns;
    }



    public List<Card> getHand() {
        return hand;
    }

    public void draw(Card card){
        addToHand(card);
        remainingTurns --;
    }
    public void addToHand(Card card){
        hand.add(card);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //used in the shuffle card
    public boolean shouldContinueShuffling() {
        // This method prompts the player to decide whether to continue shuffling
        return gameController.getView().shouldContinueShuffle();
    }

    public void removeFromHand(Card card) {
        hand.remove(card);
    }

    }

