package Server.Control;

import Server.Model.ServerUnavailableException;

import java.io.*;
import java.net.Socket;
import java.util.Arrays;

public class Clienthandler implements Runnable{
    private BufferedReader in;
    private BufferedWriter out;
    private Socket sock;
    private String clientName;
    private int cardToPlay;

    /** The connected HotelServer */
    private GameServer srv;

    /** Name of this ClientHandler */
    private String name;
    public Clienthandler(Socket sock, GameServer srv, String name) {
        try {
            in = new BufferedReader(
                    new InputStreamReader(sock.getInputStream()));
            out = new BufferedWriter(
                    new OutputStreamWriter(sock.getOutputStream()));
            this.sock = sock;
            this.srv = srv;
            this.name = name;
        } catch (IOException e) {
            shutdown();
        }
    }

    public void setCardToPlay(int cardToPlay) {
        this.cardToPlay = cardToPlay;
    }

    public int getCardToPlay() {
        return cardToPlay;
    }

    public String getName() {
        return name;
    }

    public String getClientName() {
        return clientName;
    }

    public BufferedReader getIn() {
        return in;
    }

    public BufferedWriter getOut() {
        return out;
    }

    /**
     * listens to client input regarding setting up the connection and forwards the input to the
     * {@link #handleCommand(String)} method, until the server game is full.
     */
    public void run() {
        String msg;
        try {
            msg = in.readLine();
            while (msg != null) {
                System.out.println("> [" + name + "] Incoming: " + msg);
                handleCommand(msg);
                msg = in.readLine();
                if(srv.isGameIsFull()){
                    break;
                }
            }

        } catch (IOException e) {
            shutdown();
        }
    }

    public void handleCommand(String msg) throws IOException{
        String[] split = msg.split("\\|");
        String command = "";
        String parameter1 = "";
        String parameter2 = "";
        if(split.length <1){
            System.out.println("Invalid input");
        }
        else{
            command = split[0];
        }

        switch(command) {
            case "HA":
                srv.acceptHandshake(this);
                break;
            case "AN":
                if(split.length>1){
                    clientName = split[1];
                    if(this.name.equals("Client 01")) {
                        srv.welcome(this);
                    }
                    else{
                        srv.welcome(this);
                    }
                }
                break;
        }

    }


    //method used to ask the client which card they want to play. It then reads the clients response, and returns the relevant part as an int to the server
    //if it cannot be parsed to an int, a message is sent to the client informing them about the error and the method calls itself again.
    public int askForCard(String message){
        sendResponse(Server.ServerAction.PC+"|"+ message);
        try {
            String response = in.readLine();
            String[] split = response.split("\\|");
            try {
                return Integer.parseInt(split[1]);
            }
            catch (NumberFormatException e){
                sendResponse(Server.ServerAction.PR + "|" + "Please only enter numbers");
                askForCard(message);
                return -99;
            }
        } catch (IOException e) {
            System.out.println("A server IO error occurred: "
                    + e.getMessage());
            shutdown();
            return -99;
        }
    }


    //used for the case a special combo with three cards is played. prompts the player to input a String, reads the answer from the client
    //and returns the relevant part to the server
    public String specialCombosThree(String message){
        sendResponse(Server.ServerAction.PC + "|"+message);
        try {
            String response = in.readLine();
            String[] split = response.split("\\|");
            return split[1];
        }
        catch (IOException e){
            System.out.println("A server IO error occurred: "
                    + e.getMessage());
            shutdown();
            return "ERROR";
        }
    }


    //same principle as the askForCard and specialCombosThree method, only for favour cards
    public int favourCard(String message){
        sendResponse(Server.ServerAction.FC + "|" + message);
        try {
            String response = in.readLine();
            String[] split = response.split("\\|");
            return Integer.parseInt(split[1]);
        } catch (IOException e) {
            System.out.println("A server IO error occurred: "
                    + e.getMessage());
            shutdown();
           // srv.broadcastMessage("the other player disconnected, you won!");
            return -99;
        }
    }

    //asks the client a boolean question, returns the answer to the client
    public String booleanQuestion(String message){
            sendResponse(Server.ServerAction.BQ + "|" + message);
            try {
                String response = in.readLine();
                String[] split = response.split("\\|");
                String answer = split[1].toLowerCase();
                return answer;
            } catch (IOException e) {
                System.out.println("A server IO error occurred: " + e.getMessage());
                shutdown();
                return "ERROR";
            }
    }

    //same principle as the booleanQuestion method
    public String yupCard(String message){
        sendResponse(Server.ServerAction.PC+"|"+message);
        try {
            String response = in.readLine();
            String[] split = response.split("\\|");
            return split[1];
        }
        catch(IOException e){
            System.out.println("A server IO error occured: "
                    + e.getMessage());
            shutdown();
            return "ERROR";
        }
    }

    //sends a response to the client
    public void sendResponse(String response) {
        try {
            out.write(response);
            out.newLine();
            out.flush();
        }
        catch (IOException e){
            System.out.println("A server IO error occurred in sedn Response: "
                    + e.getMessage());
            shutdown();
        }
    }



     //Shut down the connection to this client by closing the socket and
     //the In- and OutputStreams.
    private void shutdown() {
        System.out.println("> [" + name + "] Shutting down.");
        try {
            in.close();
            out.close();
            sock.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        srv.removeClient(this);
    }
}
