package Server.Control;


import Server.View.GameServerTUI;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class GameServer implements Server {

    private ServerSocket ssock;
    private List<Clienthandler> player;
    private int next_client_no;
    GameServerTUI view;
    private GameController gameController;
    private static final String GAMENAME = "Welcome to exploding Kittens";
    private static final String WELCOME_MESSAGE = "Welcome to the gameserver of Exploding Kittens!";
    private boolean gameIsFull= false;

    public GameServer(){
        player = new ArrayList<>();
        view = new GameServerTUI();
        next_client_no = 1;
        gameController = new GameController(this);

    }

    public boolean isGameIsFull() {
        return gameIsFull;
    }


    @Override
    public synchronized void acceptHandshake(Clienthandler client) {
        try {
            client.getOut().write(String.valueOf(ServerAction.HE));
            client.getOut().newLine();
            client.getOut().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public synchronized void gameStart() {
        //broadcastMessage(String.valueOf(ServerAction.GS));
        broadcastMessage("The game has started!");
        broadcastMessage(player.get(0).getClientName() + " will start.");
        gameController.setUpGame();
        broadcastMessage(String.valueOf(ServerAction.EOT));
        gameController.startGame();

    }

    //sends a message to the current player
    public synchronized void sendToPlayer(String message){
        Clienthandler currentPLayer = whoIsCurrentPlayer();
        try {
            currentPLayer.sendResponse(ServerAction.PR + "|" + message);
            currentPLayer.sendResponse(String.valueOf(ServerAction.EOT));
        }
        catch(NullPointerException e){
            gameController.setGameRunning(false);
        }
    }

    //send a message to the opposing player
    public synchronized void sendToOpposingPlayer(String message){
        Clienthandler opposingPlayer = whoIsOpposingPlayer();
        try {
            opposingPlayer.sendResponse(ServerAction.PR + "|" + message);
            opposingPlayer.sendResponse(String.valueOf(ServerAction.EOT));
        }
        catch (NullPointerException e){
            gameController.setGameRunning(false);
        }
    }

    //determines the opposing player, asks the corresponding clienthandler to ask a boolean question with the specified message, and returns the answer
    public synchronized String nopeCard(String message){
        Clienthandler opposingPLayer = whoIsOpposingPlayer();
        try {
            return opposingPLayer.booleanQuestion(message);
        }
        catch (NullPointerException e){
            return "ERROR";
        }
    }


    //determines the current player, asks the corresponding clienthandler to ask a boolean question with the specified message, and returns the answer
    public synchronized String yupCard(String message){
        Clienthandler currentPlayer = whoIsCurrentPlayer();
        try {
            return currentPlayer.yupCard(message);
        }
        catch(NullPointerException e){
            return "ERROR";
        }
    }

    //determines the opposing player, asks the corresponding clienthandler to ask a boolean question with the specified message, and returns the answer
    public synchronized String shuffleCard(String message){
        Clienthandler opposingPlayer = whoIsOpposingPlayer();
        try {
            return opposingPlayer.booleanQuestion(message);
        }
        catch(NullPointerException e){
            return "ERROR";
        }
    }


    //determines the current player, asks the corresponding clienthandler to ask a question with the specified message, and returns the answer as an integer
    public synchronized int askForCard(String message){
        Clienthandler currentPlayer = whoIsCurrentPlayer();
        try {
            return currentPlayer.askForCard(message);
        }
        catch (NullPointerException e) {
            return -99;
        }
    }

    //determines the opposing player, asks the corresponding clienthandler to ask a question with the specified message, and returns the answer as an integer
    public synchronized int favourCard(String message){
        Clienthandler opposingPLayer = whoIsOpposingPlayer();
        try {
            return opposingPLayer.favourCard(message);
        }
        catch(NullPointerException e){
            return -99;
        }
    }


    //determines the current player, asks the corresponding clienthandler to ask a question with the specified message, and returns the answer as a String
    public synchronized String specialCombosThree(String message){
        Clienthandler currentPlayer = whoIsCurrentPlayer();
        try {
            return currentPlayer.specialCombosThree(message);
        }
        catch(NullPointerException e){
            return "ERROR";
        }
    }


    //determines which clienthandler is the opposing player by comparing the name of both clienthandlers with the name of the opposing player and returns the matching one
    public synchronized Clienthandler whoIsOpposingPlayer(){
        String opposing = gameController.getOpposingPlayer().getName();
        if(player.get(0).getClientName().equals(opposing)){
            try{
                return player.get(0);
            }
            catch (IndexOutOfBoundsException e){
                System.out.println("A player has disconnected, the game is over.");
                return null;
            }
        }
        else {
            try{
                return player.get(1);
            }
            catch (IndexOutOfBoundsException e){
                System.out.println("A player has disconnected, the game is over");
                return null;
            }
        }
    }


    //determines which clienthandler is the current player by comparing the name of both clienthandlers with the name of the current player and returns the matching one
    public synchronized Clienthandler whoIsCurrentPlayer(){
        String currentName = gameController.getCurrentPlayer().getName();
        if(player.get(0).getClientName().equals(currentName)){
            try{
            return player.get(0);
            }
            catch (IndexOutOfBoundsException e){
                System.out.println("A player has disconnected, the game is over.");
                return null;
            }
        }
        else {
            try{
                return player.get(1);
            }
            catch (IndexOutOfBoundsException e){
                System.out.println("A player has disconnected, the game is over.");
                return null;
            }
        }
    }


    //assigns to the clienthandlers either a "current" or "opposing" flag. Necessary to make sure their inputs are only read when prompted.
    public synchronized void assignCurrentPlayer(){
        Clienthandler currentPlayer = whoIsCurrentPlayer();
        Clienthandler opposingPlayer = whoIsOpposingPlayer();
        try {
            currentPlayer.sendResponse(String.valueOf(ServerAction.SC));
        }
        catch (NullPointerException e){
            gameController.setGameRunning(false);
        }
        try {
            opposingPlayer.sendResponse(String.valueOf(ServerAction.SO));
        }
        catch (NullPointerException e){
            gameController.setGameRunning(false);
        }
    }

    @Override
    public void gameEnd() {

    }

    @Override
    public void informAdmin(String message) {

    }

    @Override
    public synchronized void broadcastMessage(String message) {
        if (!message.equals(String.valueOf(ServerAction.EOT))) {
            if (!player.isEmpty()) {
                player.get(0).sendResponse(ServerAction.PR + "|" + message);
                player.get(0).sendResponse(String.valueOf(ServerAction.EOT));
            }
            if (player.size() > 1) {
                player.get(1).sendResponse(ServerAction.PR + "|" + message);
                try {
                    player.get(1).sendResponse(String.valueOf(ServerAction.EOT));
                }
                catch(IndexOutOfBoundsException e){
                    System.out.println("Player is not connected anymore.");
                }
            }
        }

    }

    @Override
    public void roundStarted() {

    }

    @Override
    public void gameWinner(Clienthandler client) {

    }

    @Override
    public void explode(Clienthandler client) {

    }

    @Override
    public void diffuseCheck(Clienthandler client) {

    }


    @Override
    public void drawCard(Clienthandler client) {

    }


    //checks if the clienthandler that is passed as an argument is the first client, if yes it asks if they want to play against a computer player. If it is the second client, it wil not ask
    //in both cases, it will assign the clientName (the name chosen by the player) to the player object of the gameController.
    @Override
    public synchronized void welcome(Clienthandler client) {
        String playerName = client.getClientName();
        if(client.getName().equalsIgnoreCase("Client 01")){
            gameController.getDeck().setUpPlayer1(playerName);
            client.sendResponse(ServerAction.WE + "|" +  WELCOME_MESSAGE);
            client.sendResponse(String.valueOf(ServerAction.COM));
            String response = null;
            try {
                response = client.getIn().readLine();
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
            assert response != null;
            String[] split = response.split("\\|");
            if(split[1].equals("true")){
                gameIsFull = true;
                gameController.getDeck().setUpComputerPlayer();
                gameStart();
            }
        }
        else if(client.getName().equalsIgnoreCase("Client 02")){
            gameController.getDeck().setUpPlayer2(playerName);
            client.sendResponse(ServerAction.WE + "|" +  WELCOME_MESSAGE);
            gameIsFull = true;
            gameStart();
        }
    }




    public void run() {
        boolean openNewSocket = true;
        while (openNewSocket) {
            try {
                setup();

                while (!gameIsFull) {
                    Socket sock = ssock.accept();
                    String name = "Client "
                            + String.format("%02d", next_client_no++);
                    view.showMessage("New client [" + name + "] connected!");
                    Clienthandler handler =
                            new Clienthandler(sock, this, name);
                    new Thread(handler).start();
                    player.add(handler);
                }

            } catch (ExitProgram e1) {
                // If setup() throws an ExitProgram exception,
                // stop the program.
                openNewSocket = false;
            } catch (IOException e) {
                System.out.println("A server IO error occurred in server: "
                        + e.getMessage());
                broadcastMessage("The other player disconnected, you won!");
                break;

            }
        }
        view.showMessage("See you later!");
    }

    public void setup() throws ExitProgram {


        ssock = null;
        while (ssock == null) {
            int port = view.getInt("Please enter the server port.");
            // try to open a new ServerSocket
            try {
                view.showMessage("Attempting to open a socket at 127.0.0.1 "
                        + "on port " + port + "...");
                ssock = new ServerSocket(port, 0,
                        InetAddress.getByName("127.0.0.1"));
                view.showMessage("Server started at port " + port);
            } catch (IOException e) {
                view.showMessage("ERROR: could not create a socket on "
                        + "127.0.0.1" + " and port " + port + ".");

                if (!view.getBoolean("Do you want to try again?")) {
                    throw new ExitProgram("User indicated to exit the "
                            + "program.");
                }
            }
        }
    }



    public void removeClient(Clienthandler client) {
        this.player.remove(client);
    }


    public static void main(String[] args) {
        GameServer server = new GameServer();
        System.out.println(GAMENAME);
        server.run();
    }


}
