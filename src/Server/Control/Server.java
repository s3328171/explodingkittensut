package Server.Control;

// methods for server side operations
public interface Server {
    enum Error {
        ALREADY_LOGGED_IN(1, "Client is already logged in"),
        INVALID_USERNAME(2, "The username is invalid"),
        INVALID_MOVE(3, "The move is not allowed"),
        DISCONNECTED_USER(4, "The user has been disconnected");

        private final int code;
        private final String description;

        /**
         * @param code: The error code identifier
         * @param description: The error description
         */
        Error(int code, String description){
            this.code = code;
            this.description = description;
        }
    }
    /**
     * Enum of server commands abbreviations with full command name and descriptions.
     */
    enum ServerAction{
        //i added hello command, print message, EOT, playCard, com, SetCurrentPlayer, SetOpposingPlayer, NopeCard, FavourCard
        BQ("BooleanQuestions", "handles booleanQuestions"),
        FC("FavourCard", "handles FavourCard"),
        SC("Set current player", "assigns a client to be current player"),
        SO("Set opposing player", "assigns a client to opposing player"),
        COM("Computer player", "asks the player if they want to play agianst a computer player"),
        PC("Play card", "informs the client that they have to play a card"),
        EOT("End of transmission", "lets the client know the transmission is over"),
        PR("Print message", "tells the client to print the following message"),
        HE("Handshake", "Shakes hands with client"),
        CL("GetClient", "Client sends its object to the server"),
        WE("WELCOME", "Sends welcome message to client"),
        GS("GAME_START", "Starts the game"),
        DC("DEAL_CARD", "Deals card to client"),
        DF("DIFFUSE_CHECK", "Checks if client has a diffuse card"),
        EC("ELIMINATE_CLIENT", "Eliminates a client from the game"),
        EG("END_GAME", "Ends the current game"),
        AW("ANNOUNCE_WINNER", "Announces the game winner");

        private final String command;
        private final String description;

        ServerAction(String command, String description) {
            this.command = command;
            this.description = description;
        }
    }

    /**
     * Accepts handshake from the client
     * @param client: the client who initializes the handshake
     */
    void acceptHandshake(Clienthandler client);

    /**
     * Sends a welcome message to the client
     */
    void welcome(Clienthandler client);

    /**
     * Starts the game once there are enough players
     */
    void gameStart();

    /**
     * Deals the top card to the client
     */
    void drawCard(Clienthandler client);

    /**
     * Checks if the client has a diffuse card
     */
    void diffuseCheck(Clienthandler client);

    /**
     * Eliminates the client from the game
     */
    void explode(Clienthandler client);

    /**
     * Ends the game
     */
    void gameEnd();

    /**
     * Determines and announces the winner of the game
     */
    void gameWinner(Clienthandler client);

    /**
     * Informs an admin about an event
     * @param message: the message to send the admin
     */
    void informAdmin(String message);

    /**
     * Sends a global message to all clients
     */
    void broadcastMessage(String message);

    /**
     * Notifies that a new round has started
     */
    void roundStarted();

}
