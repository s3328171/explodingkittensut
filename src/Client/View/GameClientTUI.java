package Client.View;

import Client.Control.Client;
import Client.Control.GameClient;
import Server.Control.Server;
import Server.Control.ExitProgram;
import Server.Model.ServerUnavailableException;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Scanner;

public class GameClientTUI {
    private GameClient client;
    private Scanner scanner;
    private String serverMessage = "";
    private boolean currentPlayer = true;
    private boolean opposingPlayerNeedsToGiveInput = false;
    private boolean computerPLayerQuestion;

    public GameClientTUI(GameClient client){
        this.client = client;
        this.scanner = new Scanner(System.in);
    }

/**
    public void start() throws ServerUnavailableException {
        while(true){
            //System.out.println("test start client TUI");
            serverMessage = client.readLineFromServer();

            while (serverMessage != null) {
                handleServerInput(serverMessage);

                //System.out.println("test inside while liip in start");
                serverMessage = client.readLineFromServer();
            }
**/
/**
            if(serverMessage.startsWith(String.valueOf(Server.ServerAction.PR))){
                System.out.println("Goes into PR");
                showMessage(serverMessage.substring(3));
                serverMessage = client.readLineFromServer();
                while(!serverMessage.equals(String.valueOf(Server.ServerAction.EOT))){
                    //System.out.println("While loop of PR");
                    if(serverMessage.startsWith(String.valueOf(Server.ServerAction.PR))){
                        showMessage(serverMessage.substring(3));
                    }
                    else{
                        showMessage(serverMessage);
                    }
                    serverMessage = client.readLineFromServer();
                }
            }
            else if (serverMessage.startsWith(String.valueOf(Server.ServerAction.PC))) {
                showMessage(serverMessage.substring(3));
                System.out.println("Test if it goes into pc");
                //serverMessage = client.readLineFromServer();
                /**
                while(!serverMessage.equals(String.valueOf(Server.ServerAction.EOT))){
                    System.out.println("inside the while loop of the cleutTUi");
                    showMessage(serverMessage);
                    serverMessage = client.readLineFromServer();
                }

                System.out.println("test above scanner in client");
                input = scanner.nextLine();

                try {
                    handleUserInput(input);
                } catch (ExitProgram e) {
                    showMessage("exiting program");
                    break;
                }
            }

        }

    }
**/
    public void handleServerInput(String serverInput) throws ServerUnavailableException {
        String[] split = serverInput.split("\\|");
        //System.out.println(Arrays.toString(split));
        String command = "";
        String parameter1 = "";
        String parameter2 = "";
        String input = "";
        command = split[0];
        if(split.length >=2) {
            parameter1 = split[1];
        }
        if(split.length >=3){
            parameter2 = split[2];
        }
        //System.out.println("test clientTUI handleServerInput " + serverInput);
        //System.out.println("Command : " + command);
        switch(command){
            case "PR":
                //System.out.println("Servermessage: " + serverMessage);
                //System.out.println("paramter1: " + parameter1);
                showMessage(parameter1);
                serverMessage = client.readLineFromServer();
                while(serverMessage != null && !serverMessage.equals(String.valueOf(Server.ServerAction.EOT))){
                //    System.out.println("While loop of PR, server message: " + serverMessage);
                    if(serverMessage.startsWith(String.valueOf(Server.ServerAction.PR))){
                        showMessage(serverMessage.substring(3));
                    }
                    else{
                        showMessage(serverMessage);
                    }

                    serverMessage = client.readLineFromServer();
                }

                break;
            case "PC":
                //System.out.println("parameter1 in PC: " + parameter1);
                showMessage(parameter1);
                serverMessage = parameter1;
                //System.out.println("Test if it goes into pc");
                //System.out.println("test above scanner in client");
                //input = scanner.nextLine();
                //System.out.println(serverMessage + "serbermessage");
                break;
            case "COM":
                computerPLayerQuestion = true;
                showMessage("Do you want to play against a computerPlayer?");
                break;
            case  "SC":
                currentPlayer = true;
                break;
            case "SO":
                currentPlayer = false;
                break;
            case "BQ":
                opposingPlayerNeedsToGiveInput = true;
                //System.out.println("test booleanQzestin " + opposingPlayerNeedsToGiveInput);
                showMessage(parameter1);
                break;
            case "FC":
                opposingPlayerNeedsToGiveInput = true;
                //System.out.println("test favour " + opposingPlayerNeedsToGiveInput);
                showMessage(parameter1);
                break;
        }
    }

    public void handleUserInput(String input) throws ExitProgram, ServerUnavailableException{
            String[] split = input.split(" ");
            String command = "";
            String parameter1 = "";
            String parameter2 = "";
            if (split.length < 1) {
                System.out.println("Invalid input");
                printHelpMenu();
            } else {
                command = split[0];
            }
            if (split.length >= 2) {
                parameter1 = split[1];
            }
            if (split.length >= 3) {
                parameter2 = split[2];
            }
            switch (command.toUpperCase()) {
                case "PC":
                    //System.out.println(serverMessage + "serbermessage");
                    //System.out.println("enters pc in handle user input");
                    if(!currentPlayer){
                        showMessage("It is not your turn!");
                    }
                    else {
                        try {
                            client.playCard(Integer.parseInt(parameter1));
                            break;
                        } catch (NumberFormatException e) {
                            showMessage("Please only input valid numbers!");
                            break;
                        }

                    }
                    break;
                case "BQ":
                    //System.out.println("enters NP in handle user input " + opposingPlayerNeedsToGiveInput);
                    if(opposingPlayerNeedsToGiveInput||currentPlayer){
                        client.answerBooleanQuestion(parameter1);
                    }
                    else {
                        showMessage("It is not your turn!");
                    }
                    break;
                case "SC":
                    //System.out.println("enters SC in handle user input");
                    client.specialCombosThree(parameter1);
                    break;
                case "FC":
                   // System.out.println("enters FC in handle user input");
                    if(!opposingPlayerNeedsToGiveInput){
                        showMessage("It is not your turn!");
                    }
                    else {
                        try {
                            client.answerFavourCard(Integer.parseInt(parameter1));
                            opposingPlayerNeedsToGiveInput = false;
                            break;
                        } catch (NumberFormatException e) {
                            showMessage("Please only input valid numbers!");
                            break;
                        }

                    }
                    break;
                case "YES":
                    if(computerPLayerQuestion){
                        client.sendMessage(Client.Command.COM + "|" + true);
                    }
                    break;
                case "NO":
                    if(computerPLayerQuestion){
                        client.sendMessage(Client.Command.COM + "|" + false);
                        showMessage("Okay, the game will start as soon as a second player has connected");
                    }
            }


    }

    public void setOpposingPlayerNeedsToGiveInput(boolean opposingPlayerNeedsToGiveInput) {
        this.opposingPlayerNeedsToGiveInput = opposingPlayerNeedsToGiveInput;
    }

    public int getInput(){
        int answer = Integer.parseInt(scanner.nextLine());
        return answer;
    }

    public void showMessage(String message) {
        System.out.println(message);
    }

    public InetAddress getIp() {
        boolean isvalidIP = false;
        InetAddress ipAddress = null;
        while(!isvalidIP){
            try {
                System.out.println("Enter the IP address: ");
                String ipString = scanner.nextLine();
                ipAddress = InetAddress.getByName(ipString);
                isvalidIP = true;
            }catch (UnknownHostException e){
                showMessage("Invalid IP address. Please try again.");
            }
        }
        return ipAddress;
    }

    public String getString(String question) {
        //showMessage("please input a string");
        showMessage(question);
        return scanner.nextLine();
    }


    public int getInt(String question) {
        int result = 0;
        boolean validInput = false;
        while (!validInput) {
            try {
                showMessage(question);
                result = Integer.parseInt(scanner.nextLine());
                validInput = true;
            } catch (NumberFormatException e) {
                showMessage("Invalid input. Please enter a valid integer.");
            }
        }
        return result;
    }


    public boolean getBoolean(String question) {
        while (true) {
            showMessage(question + " (yes/no)");
            String userInput = scanner.nextLine().toLowerCase();

            if ("yes".equals(userInput) || "no".equals(userInput)) {
                //System.out.println(userInput + " in getBoolean");
                return userInput.equals("yes");
            } else {
                System.out.println("Invalid input. Please enter 'yes' or 'no'.");
            }
        }
    }


    public void printHelpMenu() {
        showMessage("Get help");
    }


}
