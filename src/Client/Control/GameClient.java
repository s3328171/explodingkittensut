package Client.Control;

import Client.View.GameClientTUI;
import Server.Control.Server;
import Server.Control.ExitProgram;
import Server.Model.ServerUnavailableException;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class GameClient implements Client, Runnable{

    private Socket serverSock;
    private BufferedReader in;
    private BufferedWriter out;
    private GameClientTUI tui;
    private String name;
    private String host;

    public GameClient(){
        this.tui = new GameClientTUI(this);
    }


    public void start() {
        try {
            createConnection();
            handshake();
            getClientName();
            announces(name);

            //System.out.println("starts TUI");
            //tui.start();
        } catch (ExitProgram e) {
            tui.showMessage(e.getMessage());
        }
        //finally {
        //    closeConnection();
        //}
    }


    @Override
    public void handshake() {

        try {
            sendMessage(String.valueOf(Command.HA));
            String response = readLineFromServer();
            if(!response.equals(String.valueOf(Server.ServerAction.HE))){
                tui.showMessage("ServerResponse is invalid");
            }
            else{
                tui.showMessage("Successfully connected to server");
            }
        }
        catch (ServerUnavailableException e){
            tui.showMessage(e.getMessage());
        }


    }

    public Client getClient(){
        return this;
    }




    public void getClientName(){
        name = tui.getString("What is your name?");
        while(name.equals("")){
            name = tui.getString("Please enter a name");
        }
    }

    @Override
    public void announces(String username) {
        try {
            sendMessage(Command.AN+ "|"+username);
            String response = readLineFromServer();
            if(!response.startsWith(String.valueOf(Server.ServerAction.WE))){
                tui.showMessage("Server Response is invalid");
            }
            else{
                tui.showMessage(response.substring(3));
            }
        }
        catch (ServerUnavailableException e) {
            System.out.println("announcement fail");;
        }
    }

    public void specialCombosThree(String answer){
        try{
            sendMessage(Command.PC + "|"+ answer);
        }catch (ServerUnavailableException e){
            tui.showMessage(e.getMessage());
        }
    }
    public void answerFavourCard(int answer){
        try {
            sendMessage(Command.PC + "|" + answer);
            tui.setOpposingPlayerNeedsToGiveInput(false);
        }
        catch (ServerUnavailableException e){
            tui.showMessage(e.getMessage());
        }
    }

    public void answerBooleanQuestion(String answer){
        if(answer.equalsIgnoreCase("yes") || answer.equalsIgnoreCase("no")){
            try {
                sendMessage(Command.PC + "|" + answer);
                tui.setOpposingPlayerNeedsToGiveInput(false);
            } catch (ServerUnavailableException e) {
                tui.showMessage(e.getMessage());
            }
        }
        else{
            tui.showMessage("Please only answer yes or no.");
        }
    }

    public void playCard(int index){
        try {
           // System.out.println("test in playCard");
            sendMessage(Command.PC + "|" + index);
            //sendMessage(Command.PC + "|" + index);
            //sendMessage(String.valueOf(Command.EOT));
        } catch (ServerUnavailableException e) {
            tui.showMessage(e.getMessage());
        }
    }
    @Override
    public void doMovePlayCard(String type, String name) {

    }


    @Override
    public void doMoveEndTurn() {

    }

    @Override
    public void requestGameState() {

    }

    public void getHost(){
        String answer = tui.getString("Do you want to use a local connection?");
        if(answer.equalsIgnoreCase("yes")){
            host = "127.0.0.1";
        }
        else if(answer.equalsIgnoreCase("no")){
            host = tui.getIp().toString();
        }
        else{
            tui.showMessage("Please answer yes or no");
            getHost();
        }
    }

    public void createConnection() throws ExitProgram {
        clearConnection();
        while (serverSock == null) {
            getHost();
            int port = tui.getInt("Please enter the port you want to connect to");

            // try to open a Socket to the server
            try {
                InetAddress addr = InetAddress.getByName(host);
                System.out.println("Attempting to connect to " + addr + ":"
                        + port + "...");
                serverSock = new Socket(addr, port);
                in = new BufferedReader(new InputStreamReader(
                        serverSock.getInputStream()));
                out = new BufferedWriter(new OutputStreamWriter(
                        serverSock.getOutputStream()));
            } catch (IOException e) {
                System.out.println("ERROR: could not create a socket on "
                        + host + " and port " + port + ".");

                //Do you want to try again? (ask user, to be implemented)
                if(false) {
                    throw new ExitProgram("User indicated to exit.");
                }
            }
        }
    }

    public void clearConnection() {
        serverSock = null;
        in = null;
        out = null;
    }

    public synchronized void sendMessage(String msg)
            throws ServerUnavailableException {
        if (out != null) {
            try {
                out.write(msg);
                out.newLine();
                out.flush();
            } catch (IOException e) {
                System.out.println(e.getMessage());
                throw new ServerUnavailableException("Could not write "
                        + "to server.");
            }
        } else {
            throw new ServerUnavailableException("Could not write "
                    + "to server.");
        }
    }


    public String readLineFromServer()
            throws ServerUnavailableException {
        if (in != null) {
            try {
                // Read and return answer from Server
                String answer = in.readLine();
                if (answer == null) {
                    throw new ServerUnavailableException("Could not read "
                            + "from server.");
                }
                return answer;
            } catch (IOException e) {
                throw new ServerUnavailableException("Could not read "
                        + "from server.");
            }
        } else {
            throw new ServerUnavailableException("Could not read "
                    + "from server.");
        }
    }

    public String readMultipleLinesFromServer()
            throws ServerUnavailableException {
        if (in != null) {
            try {
                // Read and return answer from Server
                StringBuilder sb = new StringBuilder();
                for (String line = in.readLine(); line != null
                        && !line.equals(Server.ServerAction.EOT);
                     line = in.readLine()) {
                    sb.append(line + System.lineSeparator());
                }
                return sb.toString();
            } catch (IOException e) {
                throw new ServerUnavailableException("Could not read "
                        + "from server.");
            }
        } else {
            throw new ServerUnavailableException("Could not read "
                    + "from server.");
        }
    }

    public void closeConnection() {
        tui.showMessage("Closing the connection...");
        try {
            in.close();
            out.close();
            serverSock.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void continuouslyWriting() throws ExitProgram, ServerUnavailableException {
        Scanner scanner = new Scanner(System.in);
        String userInput;

        while ((userInput = scanner.nextLine()) != null) {
            //System.out.println("while loop writing");
            tui.handleUserInput(userInput);
        }
    }

    public static void main(String[] args) throws ExitProgram, ServerUnavailableException {
        GameClient client = new GameClient();
        client.start();
        new Thread(client).start();
        client.continuouslyWriting();


    }

    @Override
    public void run() {
        //System.out.println("run method client");
        String msg;
        try {
            while((msg = in.readLine()) != null) {
                //System.out.println( "While loop run message for server input: " + msg);
                tui.handleServerInput(msg);
            }
        } catch (IOException|ServerUnavailableException e) {
            tui.showMessage(e.getMessage());
        }
    }
}
