package Client.OfflineGame.Control;

import Client.OfflineGame.Modell.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class GameControllerTest {
    private GameController gameController;

    @BeforeEach
    public void setUp() {
        gameController = new GameController();
        gameController.setUpGame();
    }

    @Test
    public void testSwitchPlayers() {
        gameController = new GameController();
        gameController.setUpGame();

        Player currentPlayer = gameController.getCurrentPlayer();
        Player opposingPlayer = gameController.getOpposingPlayer();

        gameController.switchPlayers();

        // Check if players are switched
        assertNotEquals(currentPlayer, gameController.getCurrentPlayer());
        assertNotEquals(opposingPlayer, gameController.getOpposingPlayer());

        // Check if switching again brings back the original players
        gameController.switchPlayers();
        assertEquals(currentPlayer, gameController.getCurrentPlayer());
        assertEquals(opposingPlayer, gameController.getOpposingPlayer());
    }

    @Test
    public void testCheckAttackTurn() {
        gameController = new GameController();
        gameController.setUpGame();
        // Assuming initial state is not an attack turn
        assertFalse(gameController.isAttackFlag());

        // Assuming opposing player has 2 remaining turns
        gameController.getOpposingPlayer().setRemainingTurns(2);
        gameController.checkAttackTurn();
        assertTrue(gameController.isAttackFlag());

        // Assuming opposing player has 1 remaining turn
        gameController.getOpposingPlayer().setRemainingTurns(1);
        gameController.checkAttackTurn();
        assertFalse(gameController.isAttackFlag());
    }

    @Test
    public void testValidCardValidity() {
        gameController = new GameController();
        gameController.setUpGame();

        // Test with a valid card choice
        gameController.testValidity(2);
        assertTrue(gameController.getPlayValidity());
    }

    //Defuse can only be choice 1 (initially) because we've created the code to always put DefuseCard in index 1
    //when starting the game.
    @Test
    public void testDefuseCardValidity(){
        gameController = new GameController();
        gameController.setUpGame();

        gameController.getCurrentPlayer().addToHand(new DefuseCard());

        gameController.testValidity(1);
        assertFalse(gameController.getPlayValidity());
    }

    @Test
    public void testNopeCardValidity() {
        gameController = new GameController();
        gameController.setUpGame();

        // Manually create a hand with DefuseCard at index 1 and NopeCard at index 2
        List<Card> playerHand = new ArrayList<>();
        playerHand.add(new DefuseCard());
        NopeCard nopeCard = new NopeCard(gameController.getDeck(), gameController, gameController.getView());
        playerHand.add(nopeCard);
        gameController.getCurrentPlayer().setHand(playerHand);

        gameController.testValidity(2);
        assertFalse(gameController.getPlayValidity());
    }

    @Test
    public void testDefuseThatKitten() {
        // Mock the GameController, Deck, and Players
        GameController gameController = mock(GameController.class);
        Deck deck = mock(Deck.class);
        Player currentPlayer = mock(Player.class);
        Player opposingPlayer = mock(Player.class);

        // Create actual instances for the cards
        ExplodingKitten explodingKitten = new ExplodingKitten();
        DefuseCard defuseCard = new DefuseCard();

        // Set up mocks
        when(gameController.getDeck()).thenReturn(deck);
        when(gameController.getCurrentPlayer()).thenReturn(currentPlayer);
        when(gameController.getOpposingPlayer()).thenReturn(opposingPlayer);
        when(deck.getActivePile()).thenReturn(new ArrayList<>());

        // Add Exploding Kitten to hand
        when(deck.drawCard()).thenReturn(explodingKitten);

        // Add Defuse Card to hand
        when(currentPlayer.getHand()).thenReturn(Collections.singletonList(defuseCard));

        gameController.defuseThatKitten(explodingKitten);

    }

    @Test
    public void testDrewExplodingKittenWithDefuse() {
        // Arrange
        GameController gameController = mock(GameController.class);
        Player currentPlayer = mock(Player.class);

        // Create a real instance of ExplodingKitten
        ExplodingKitten explodingKitten = new ExplodingKitten();
        DefuseCard defuseCard = mock(DefuseCard.class);

        // Set up the player's hand to contain Exploding Kitten and Defuse Card
        when(gameController.getCurrentPlayer()).thenReturn(currentPlayer);
        when(currentPlayer.getHand()).thenReturn(Arrays.asList(explodingKitten, defuseCard));

        // Mock the behavior of defuseThatKitten since it's not the focus of this test
        doNothing().when(gameController).defuseThatKitten(any());

        // Act
        gameController.drewExplodingKitten();

    }

        // You may need to check other conditions based on your game logic
        // For example, check that the game is still running and the player's turn is still active


    @Test
    public void testDrewExplodingKittenWithoutDefuse() {

        GameController gameController = new GameController();
        gameController.setUpGame();

        // Manually add an Exploding Kitten to the current player's hand
        ExplodingKitten explodingKitten = new ExplodingKitten();
        gameController.getCurrentPlayer().addToHand(explodingKitten);

        List<Card> hand = gameController.getCurrentPlayer().getHand();

        // Check if the hand contains a DefuseCard before removing it
        boolean hasDefuse = hand.stream().anyMatch(card -> card instanceof DefuseCard);

        if (hasDefuse) {
            hand.removeIf(card -> card instanceof DefuseCard);
        }

        gameController.drewExplodingKitten();

        // Verify that the player's hand does not contain a DefuseCard
        assertFalse(gameController.getCurrentPlayer().getHand().stream().anyMatch(card -> card instanceof DefuseCard));
    }

}