package Client.OfflineGame.Control;

import Client.OfflineGame.Modell.*;
import Client.OfflineGame.View.GameView;

import java.util.ArrayList;
import java.util.List;

public class GameController {
    private Deck deck;
    private Player currentPlayer;
    private Player opposingPlayer;
    private GameView view;
    private List<Player> playerList;
    private Card activeCard;
    private boolean gameRunning = false;
    private boolean playValidity = false;
    private boolean playerTurn = false;
    private boolean draw = true;
    private boolean noped = false;
    private boolean attackFlag = false;
    private SpecialCombos specialCombos;

    public GameController() {
        this.view = new GameView(this);
        this.deck = new Deck(this, view);
        this.playerList = new ArrayList<>();
        playerList.add(deck.getPlayer1());
        playerList.add(deck.getPlayer2());
        specialCombos = new SpecialCombos("name");

    }

    public void setDeck(Deck deck) {
        this.deck = deck;
    }

    public Deck getDeck() {
        return deck;
    }

    public GameView getView() {
        return view;
    }

//    public void displayPlayerHandAfterTurn()  {
//        view.displayPlayerHand(currentPlayer.getName(), currentPlayer.getHand());
//    }

    //Activating the card on the top of the active pile
    public void activateCard() {
        activeCard = deck.getActivePile().get(deck.getActivePile().size() - 1);
        activeCard.activate();
    }

    public void startGame() {
        view.displayWelcomeMessage();
        deck.setPlayerNames();
        gameRunning = true;

        //First while loop is running the game
        while (gameRunning) {
            playerTurn = true;

            //Ensures the player has at least one remaining turn
            if (opposingPlayer.getRemainingTurns() <= 0) {
                opposingPlayer.setRemainingTurns(1);
            }
            if (currentPlayer.getRemainingTurns() <= 0) {
                currentPlayer.setRemainingTurns(1);
            }

            view.printCurrentPlayer();
            if(attackFlag){
                view.attackCardMessage();
            }
            //Loop for the current player's turn
            while (playerTurn) {
                for (int i = 0; i < currentPlayer.getRemainingTurns(); ) {
                    draw = true;
                    view.displayPlayerHand(currentPlayer.getName(), currentPlayer.getHand());
                    //Check if the drawn card is an Exploding Kitten
                    checkForExplodingKitten();
                    //debugging purp
                    System.out.println(currentPlayer.getRemainingTurns() + "(remaining turns)" + i + "(i)");
                    System.out.println("attack flag: " + attackFlag);
                    view.displayTurnOptions();

                    //Initialize choice to -1 to ensure the loop starts
                    //allowing the player to make their first choice
                    int choice = -1;

// Loop for handling player card choices
// Checks if the player has chosen to end their turn by drawing
// a card.
                    if (!(currentPlayer instanceof ComputerPlayer)) {
                        while (true) {
                            choice = view.getPlayerChoice(); // Declare 'choice' only once
                            if (choice == 0) {
                                break;  // Draw a card
                            }

                            testValidity(choice);
                            if (!playValidity) {
                                continue;
                            }

                            // Play the selected card
                            view.playerTurn(currentPlayer.getHand().get(choice-1));
                            currentPlayer.play(choice, deck);
                            checkForNope();

                            // Activating the card unless it has been noped
                            if (!noped) {
                                activateCard();
                            }

                            // Check for skip cards and remaining turns
                            if (!playerTurn) {
                                break;
                                /**
                                 if (currentPlayer.getRemainingTurns() <= 1) {
                                 break;
                                 //handles that if a skip card is played as one of the attack card turns, you only skip one of your turns
                                 } else if (deck.getActivePile().get(deck.getActivePile().size() - 1) instanceof SkipCard) {
                                 draw = false;
                                 }
                                 **/
                            }
                        }
                    }

                    //handles the turn for the computerplayer
                    if (currentPlayer instanceof ComputerPlayer){
                        if(((ComputerPlayer) currentPlayer).shouldIPlayOrShouldIDraw()){
                            choice = ((ComputerPlayer) currentPlayer).naiveMove();
                            view.computerPlayerTurn(currentPlayer.getHand().get(choice-1));
                            currentPlayer.play(choice, deck);
                            checkForNope();
                            if (!noped){
                                activateCard();
                            }
                            if(!playerTurn){
                                break;
                            }
                            view.displayPlayerHand(currentPlayer.getName(), currentPlayer.getHand());
                        }
                    }

                    //Drawing a card to end the turn
                    if (draw) {
                        currentPlayer.draw(deck.drawCard());
                        view.displayPlayerHand(currentPlayer.getName(), currentPlayer.getHand());
                        checkForExplodingKitten();
                    }
                    //debugging purp
                    System.out.println(currentPlayer.getRemainingTurns() + "(remaining turns)" + i + "(i)");
                }
                checkAttackTurn();
                switchPlayers();

                //blank lines to visually seperate the turns
                for (int i = 0; i < 3; i++) {
                    System.out.println("\n");
                }

                playerTurn = false;
            }
        }
    }

    public void switchPlayers() {
        Player temp = currentPlayer;
        currentPlayer = opposingPlayer;
        opposingPlayer = temp;
    }

    public void setUpGame(){
        deck.setUpGame();
        currentPlayer = deck.getPlayer1();
        opposingPlayer = deck.getPlayer2();
    }

    public void setDraw(boolean draw){
        this.draw = draw;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public boolean isAttackFlag() {
        return attackFlag;
    }

    public void setAttackFlag(boolean attackFlag) {
        this.attackFlag = attackFlag;
    }

    public void checkAttackTurn(){
        if(opposingPlayer.getRemainingTurns()>=2){
            attackFlag = true;
        }
        else{
            //if (currentPlayer.getRemainingTurns() == 0){
            attackFlag = false;
        }
    }

    //Tests validity of the played card
    public void testValidity(int choice) {
        if (choice == 0) {
            playValidity = true;
        } else if (choice < 0) {
            choice = choice * -1;
            Card[] comboCards = new Card[choice];

            for (int i = 0; i < choice; i++){
                int cardChoice = view.getPlayerComboChoice(i+1);

                Card selectedCard = currentPlayer.getHand().get(cardChoice - 1);
                comboCards[i] = selectedCard;
                System.out.println("Your card: " + selectedCard);
            }

            System.out.print("Cards in combo: ");
            for (Card comboCard : comboCards) {
                System.out.print(comboCard);
                System.out.print(" ");
            }

            if (choice == 2) {
                specialCombos.playPair(currentPlayer, opposingPlayer);
                for (Card card : comboCards){
                    for(Card card1 : currentPlayer.getHand()){
                        if(card.toString().equals(card1.toString())){
                            currentPlayer.getHand().remove(card1);
                            break;
                        }
                    }
                }
            } else if (choice == 3) {
                specialCombos.playTriplet(currentPlayer, opposingPlayer);
                for (Card card : comboCards){
                    for(Card card1 : currentPlayer.getHand()){
                        if(card.toString().equals(card1.toString())){
                            currentPlayer.getHand().remove(card1);
                            break;
                        }
                    }
                }
            }

        } else {
            Card selectedCard = currentPlayer.getHand().get(choice - 1);
            System.out.println("Selected Card: " + selectedCard);


            if (isInvalidCard(selectedCard)) {
                view.invalidCard();
                playValidity = false;
            } else {
                playValidity = true;
            }

        }
    }

    public boolean getPlayValidity() {
        return playValidity;
    }

    private boolean isInvalidCard(Card card) {
        return card instanceof ExplodingKitten || card instanceof DefuseCard || card instanceof NopeCard;
    }

    //Checks if the player has an exploding kitten
    public void checkForExplodingKitten() {
        if (currentPlayer.getHand().stream().anyMatch(card -> card instanceof ExplodingKitten)) {
            drewExplodingKitten();
            playerTurn = false;
        }
    }

    //Checks if the opposing player has a nope card
    private void checkForNope() {
        for (Card card : getOpposingPlayer().getHand()) {
            if (card instanceof NopeCard) {
                ((NopeCard) card).nopeYupCycle();
                break;
            }
        }
    }

    public Player getOpposingPlayer() {
        return opposingPlayer;
    }

    public void setPlayerTurn(boolean playerTurn) {
        this.playerTurn = playerTurn;
    }

    //Infroms the player that they have a defuse card, asks them
    //where they want to put it and puts it there
    public void defuseThatKitten(ExplodingKitten kitten) {
        if(deck.getDeck().isEmpty()){
            view.defuseCardEmptyDeck();
            deck.getDeck().add(kitten);
        }
        else {
            deck.getDeck().set(view.defuseCard() - 1, kitten);
        }
        for (Card card : currentPlayer.getHand()) {
            if (card instanceof DefuseCard) {
                deck.addToActivePile(card);
                currentPlayer.removeFromHand(card);
                break;
            }
        }
        view.displayPlayerHand(currentPlayer.getName(), currentPlayer.getHand());
    }

    //Checks if the player has a defsue card, removes the exploding kitten
    public void drewExplodingKitten() {
        if (currentPlayer.getHand().stream().anyMatch(card -> card instanceof DefuseCard)) {
            ExplodingKitten explode = null;
            for (Card card : currentPlayer.getHand()) {
                if (card instanceof ExplodingKitten) {
                    explode = (ExplodingKitten) card;
                    currentPlayer.getHand().remove(card);
                    break;
                }
            }

            defuseThatKitten(explode);
        } else {
            view.displayExplodedMessage();
            playerTurn = false;
            gameRunning = false;
        }
    }

    public void toggleNoped() {
        noped = !noped;
    }

    public int getUserInput(){
        return -1;
    }

    public static void main(String[] args) {
        GameController game = new GameController();
        game.setUpGame();
        game.startGame();
    }
}