package Client.OfflineGame.Modell;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Player {
    private List<Card> hand;
    private String name;
    private int remainingTurns;


    public Player(String name){
        this.name = name;
        hand = new ArrayList<>();
        remainingTurns = 1;
    }
    @Override
    public String toString(){
        return name;
    }

    public void play(int card, Deck playingDeck){
        if(hand.isEmpty()){
            System.out.println("Your hand is empty");
        }
        else{
            Card playedCard = getHand().get(card-1);
            playingDeck.addToActivePile(playedCard);
            //After turn, decrements the remaining turns
            removeFromHand(playedCard);
        }

    }


    public void addTurns(int turns) {
        remainingTurns += turns;
    }

    public int getRemainingTurns() {
        return remainingTurns;
    }

    public void resetRemainingTurns() {
        remainingTurns = 1;
    }

    public void setRemainingTurns(int remainingTurns) {
        this.remainingTurns = remainingTurns;
    }

    //public void DrawToEndTurn(List<Card> deck){
    //    if (!deck.isEmpty()) {
     //       Card drawnCard = deck.remove(0);
     //       drawnCard.setOwner(this);
     //       addToHand(drawnCard);
     //       addTurns(1);
     //   } else {
     //       System.out.println("The deck is empty");
     //   }
   // }

    public List<Card> getHand() {
        return hand;
    }

    public void draw(Card card){
        addToHand(card);
        remainingTurns --;
    }
    public void addToHand(Card card){
        hand.add(card);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //used in the shuffle card
    public boolean shouldContinueShuffling() {
        // This method prompts the player to decide whether to continue shuffling
        Scanner scanner = new Scanner(System.in);

        System.out.println("Do you want to continue shuffling? (yes/no)");
        String input = scanner.nextLine().toLowerCase();

        return input.equals("yes");
    }

    public void removeFromHand(Card card) {
        hand.remove(card);
    }

    public void setHand(List<Card> playerHand) {
        this.hand = new ArrayList<>(playerHand);
    }

}

