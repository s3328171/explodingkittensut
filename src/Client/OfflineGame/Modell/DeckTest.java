package Client.OfflineGame.Modell;

import Client.OfflineGame.Control.GameController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DeckTest extends Deck {
    Deck testDeck;
    GameController gameController;

    @BeforeEach
    public void setUp() {
        gameController = new GameController();
        testDeck = new Deck(gameController, gameController.getView());
    }

    @Test
    public void testUninitializedDeck() {
        assertEquals(42, testDeck.getDeck().size());
    }

    @Test
    public void test1() {
        testDeck.initializeDeck();
        assertEquals(45, testDeck.getDeck().size());
        int countDefuse = 0;
        int countExplode = 0;
        for (Card card : testDeck.getDeck()) {
            if (card instanceof ExplodingKitten) {
                countExplode++;
            } else if (card instanceof DefuseCard) {
                countDefuse++;
            }
        }
        assertEquals(1, countExplode);
        assertEquals(2, countDefuse);
    }

    @Test
    public void testDeckSize() {
        testDeck.initializeDeck();
        assertEquals(45, testDeck.getDeck().size());
    }

    @Test
    public void testPlayerHands() {
        testDeck.initializeDeck();
        assertEquals(1, testDeck.getPlayer1().getHand().size());
        assertEquals(1, testDeck.getPlayer2().getHand().size());
        testDeck.dealCardsToPlayer();
        assertEquals(8, testDeck.getPlayer1().getHand().size());
        assertEquals(8, testDeck.getPlayer2().getHand().size());
    }

    @Test
    public void testPlayerDefuse() {
        testDeck.initializeDeck();
        boolean player1has = false;
        boolean player2has = false;
        for (Card card : testDeck.getPlayer1().getHand()) {
            if (card instanceof DefuseCard) {
                player1has = true;
                break;
            }
        }
        for (Card card : testDeck.getPlayer2().getHand()) {
            if (card instanceof DefuseCard) {
                player2has = true;
                break;
            }
        }
        assertTrue(player1has);
        assertTrue(player2has);
    }
}