package Client.OfflineGame.Modell;

import Client.OfflineGame.Control.GameController;
import Client.OfflineGame.View.GameView;

public class NopeCard extends Card{
    private Deck deck;
    private GameController gameController;
    private GameView view;
    private boolean keepGoing = true;
    private boolean noped = false;
    public NopeCard(Deck deck, GameController gameController, GameView view){
        this.deck = deck;
        this.gameController = gameController;
        this.view = view;
    }

    @Override
    public void activate() {
        nope();
    }
    @Override
    public String toString(){
        return getClass().getSimpleName();
    }

    //checks if the opposing player has a nopeCard, and asks if it should be played. if yes, the variable "nope" in the controller is toggled
    //if the answer is no, the local variable keepGoing is set to false
    public void nope(){
        if(gameController.getOpposingPlayer().getHand().stream().anyMatch(card -> card instanceof NopeCard)){
            String answer = view.nopeHasBeenFound();
            if(answer.equals("yes")){
                gameController.toggleNoped();
                gameController.getOpposingPlayer().getHand().remove(this);
                view.noped();
                noped = true;
            }
            else{
                keepGoing = false;
            }
        }
    }
    //same as the nope() method, but for the currentplayer
    public void yup(){
        if(gameController.getCurrentPlayer().getHand().stream().anyMatch(card -> card instanceof NopeCard) && noped){
            String answer = view.yupHasBeenFound();
            if(answer.equals("yes")){
                gameController.toggleNoped();
                for(Card card : gameController.getCurrentPlayer().getHand()){
                    if(card instanceof NopeCard){
                        gameController.getCurrentPlayer().getHand().remove(card);
                        break;
                    }
                }
                //gameController.getCurrentPlayer().getHand().remove(this);
                view.yup();
            }
            else{
                keepGoing = false;
            }
        }
        else{
            keepGoing = false;
        }

    }

    //handles the cycle of nopes and yups if both players have nopeCards. if one of them doesn't have one or said no, the loop terminates
    public void nopeYupCycle(){
        while(keepGoing){
            if(gameController.getOpposingPlayer().getHand().stream().anyMatch(card -> card instanceof NopeCard)){
                nope();
                if(gameController.getCurrentPlayer().getHand().stream().anyMatch(card -> card instanceof NopeCard)){
                    yup();
                }
                else {
                    break;
                }
            }
            else{
                break;
            }

        }

    }


    //public void nopy(){
    //    Card lastActionCard = deck.getActivePile().remove(deck.getActivePile().size()-1);
    //    if (lastActionCard instanceof ExplodingKitten || lastActionCard instanceof DefuseCard) {
    //        System.out.println("You can't play a Nope Card on an Exploding Kitten or a Defuse Card.");
    //    }
    //    else{
    //        gameController.toggleNoped();
    //    }

    //}

    //public void playNope(Deck deck, Player currentPlayer) {
    //    if (deck.shouldIgnoreLastAction()) {
    //        Card lastActionCard = deck.getActivePile().remove(deck.getActivePile().size()-1);

    //        // Check if the last action should be ignored
    //        if (lastActionCard instanceof ExplodingKitten || lastActionCard instanceof DefuseCard) {
    //            System.out.println("You can't play a Nope Card on an Exploding Kitten or a Defuse Card.");
    //            if (deck.shouldIgnoreLastAction()) {
    //                System.out.println("Nope! The previously played card is ignored.");
    //                deck.acknowledgeLastAction(); // Reset the flag for the next action
    //            }
     //       }
    //    }
    //}
}
