package Client.OfflineGame.Modell;

import Client.OfflineGame.Control.GameController;
import Client.OfflineGame.View.GameView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Deck {
    public static int EXPL_KITTEN_CARDS = 1;
    public static int DEFUSE_CARDS = 4;
    public static int ATTACK_CARD = 4;
    public static int FAVOR_CARD = 4;
    public static int NOPE_CARD = 5;
    public static int SHUFFLE_CARD = 4;
    public static int SKIP_CARD = 4;
    public static int SEE_THE_FUTURE = 5;
    public static int CAT_CARDS = 4; //4 taco cats, 4 etc cats -> SPECIAL_COMBOS is not just the cat cards, it's
    // only a method to do two actions

    // Default constructor
// Default constructor
    public Deck() {
        deck = new ArrayList<>();
        explodeAndDefuse = new ArrayList<>();
        activePile = new ArrayList<>();
        discardPile = new ArrayList<>();
        player1 = new Player("defaultPlayer1");
        player2 = new Player("defaultPlayer2");
        gameController = new GameController();  // You may need a default constructor for GameController
        gameView = new GameView(gameController);  // Provide the required GameController parameter
        ignoreLastAction = false;

    }

    private List<Card> deck;
    private List<Card> explodeAndDefuse;
    private List<Card> activePile;
    private List<Card> discardPile;
    private Player player1;
    private Player player2;
    private GameController gameController;
    private GameView gameView;
    private boolean ignoreLastAction;

    /**
     * creates all the cards needed for playing the game
     */
    public Deck(GameController gameController, GameView gameView){

        deck = new ArrayList<>();
        explodeAndDefuse = new ArrayList<>();
        activePile = new ArrayList<>();
        discardPile = new ArrayList<>();
        this.gameController = gameController;
        this.gameView = gameView;

        for(int i = 1; i <= EXPL_KITTEN_CARDS; i++){
            ExplodingKitten explodingKitten = new ExplodingKitten();
            explodeAndDefuse.add(explodingKitten);
        }
        for(int i = 1; i <= DEFUSE_CARDS; i++){
            DefuseCard defuseCard = new DefuseCard();
            explodeAndDefuse.add(defuseCard);
        }
        for(int i = 1; i <= ATTACK_CARD; i++){
            AttackCard attackCard = new AttackCard(gameController, this, gameView);
            deck.add(attackCard);
        }
        for(int i = 1; i <= FAVOR_CARD; i++){
            FavorCard favorCard = new FavorCard(gameController);
            deck.add(favorCard);
        }
        for(int i = 1; i <= NOPE_CARD; i++){
            NopeCard nopeCard = new NopeCard(this, gameController, gameView);
            deck.add(nopeCard);
        }
        for(int i = 1; i <= SHUFFLE_CARD; i++){
            ShuffleCard shuffleCard = new ShuffleCard(gameController);
            deck.add(shuffleCard);
        }
        for(int i = 1; i <= SKIP_CARD; i++){
            SkipCard skipCard = new SkipCard(gameController);
            deck.add(skipCard);
        }
        for(int i = 1; i <= SEE_THE_FUTURE; i++){
            SeeFutureCard seeFutureCard = new SeeFutureCard(gameController, this);
            deck.add(seeFutureCard);
        }
        for(int i = 1; i <= CAT_CARDS; i++){
            CatCards tacoCat = new CatCards("tacoCat");
            deck.add(tacoCat);
        } //True for this and all following SPECIAL_COMBOS: they are not part of Special_Combos

        for(int i = 1; i <= CAT_CARDS; i++){
            CatCards bikiniCat = new CatCards("bikiniCat");
            deck.add(bikiniCat);
        }
        for(int i = 1; i <= CAT_CARDS; i++){
            CatCards hairyPotatoCat = new CatCards("hairyPotatoCat");
            deck.add(hairyPotatoCat);
        }
        for(int i = 1; i <= CAT_CARDS; i++){
            CatCards rainbowCat = new CatCards("rainbowCat");
            deck.add(rainbowCat);
        }

        ignoreLastAction = false;

    }

    public void ignoreLastAction(){
        ignoreLastAction = true;
    }

    public void acknowledgeLastAction(){
        ignoreLastAction = false;
    }

    public boolean shouldIgnoreLastAction(){
        return ignoreLastAction;
    }

    /**
     * initializes the deck according to game rules
     * a.k.a, adds players-1 exploding kitten cards, and two defuse cards to the deck
     * adds a defuse card to each player's hand
     */
    public void initializeDeck(){

        setUpPlayers();

        Iterator<Card> iterator = explodeAndDefuse.iterator();
        while(iterator.hasNext()){
            Card kitten = iterator.next();
            if(kitten instanceof ExplodingKitten){
                deck.add(kitten);
                iterator.remove();
                break;
            }
        }


        int defuseCounter = 0;
        iterator = explodeAndDefuse.iterator();
        while(iterator.hasNext()){
            Card defuse = iterator.next();
            if(defuseCounter == 2){
                break;
            }
            else if (defuse instanceof  DefuseCard){
                deck.add(defuse);
                iterator.remove();
                defuseCounter ++;
            }
        }
        /**
         * adds one defuse card to each of the players' hands
         */
        for (Card playerDefuse : explodeAndDefuse){
            if(playerDefuse instanceof DefuseCard){
                if(!player1.getHand().contains(playerDefuse)){
                    player1.addToHand(playerDefuse);
                    break;
                }
            }
        }
        for (Card playerDefuse : explodeAndDefuse){
            if(playerDefuse instanceof DefuseCard){
                if(!player2.getHand().contains(playerDefuse)){
                    player2.addToHand(playerDefuse);
                    break;
                }
            }
        }
        shuffle(this.deck);
    }

    public void dealCardsToPlayer(){
        for(int i = 0; i < 7; i++){
            drawWithoutExplodingKitten(player1);
            shuffle(this.deck);
            drawWithoutExplodingKitten(player2);
        }

        deck.addAll(explodeAndDefuse);
        shuffle(this.deck);

    }

    private void drawWithoutExplodingKitten(Player player) {
        Card drawnCard;
        do {
            drawnCard = drawCard();
            if(!(drawnCard instanceof ExplodingKitten)) {
                player.addToHand(drawnCard);
                break;
            } else {
                explodeAndDefuse.add(drawnCard);
            }
        } while (true);
    }

    public void setUpGame(){
        initializeDeck();
        dealCardsToPlayer();
    }


    public void setUpPlayers(){
//        boolean com = gameView.doYouWantCom();
        //boolean com = false;
        this.player1 = new Player("blank");
//        if(com){
//            this.player2 = new ComputerPlayer("blank2", gameController);
//        }
//        else {
            this.player2 = new Player("blank2");
        }
//    }
    public void setPlayerNames(){
        gameView.initializePlayerNames();
    }

    public void shuffle(List<Card> deck){
        Collections.shuffle(deck);
    }

    /**
     *
     * the .get(0) might cause issue in the future
     */
    public Card drawCard(){
        Card card = deck.get(0);
        deck.remove(card);
        return card;

    }

    public void drawPlayer1(){
        player1.addToHand(drawCard());
    }

    public void drawPlayer2(){
        player2.addToHand(drawCard());
    }


    /**
     *
     * i more and more think it makes more sense to have at least the discard pile
     * not as a class but as another list in here.
     */
    public void discard(Card card){
        discardPile.add(card);

    }

    public void addToActivePile(Card card){
        activePile.add(card);
    }

    public List<Card> getDeck(){
        return deck;
    }

    public List<Card> getDiscardPile(){
        return discardPile;
    }
    public List<Card> getActivePile(){
        return activePile;
    }

    public Player getPlayer1(){
        return player1;
    }
    public Player getPlayer2(){
        return player2;
    }

    public List<Card> getExplodeAndDefuse() {
        return explodeAndDefuse;
    }

    // Method to shuffle the draw pile (deck)
    //public void shuffleDrawPile(Player nextPlayer) {
    // Assuming this method is called when the ShuffleCard is played
    //   ShuffleCard.shuffle(deck, nextPlayer);
    //}
}