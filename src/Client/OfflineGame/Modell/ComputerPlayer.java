package Client.OfflineGame.Modell;

import Client.OfflineGame.Control.GameController;

import java.util.Random;

public class ComputerPlayer extends Player{

    GameController gameController;
    private int attackIndex;

    public ComputerPlayer(String name, GameController gameController) {
        super(name);
        this.gameController = gameController;
    }

    /**
     *  function that decides if the computerplayer draws or plays a card, com plays a card with a possibility of 75%
     * @return true if computer player is playing instead of drawing
     */
    public boolean shouldIPlayOrShouldIDraw(){
        Random random = new Random();
        double probabiltiy = random.nextDouble();
        //returns true when the computer player is playing instead of drawing
        return probabiltiy <0.75;
    }

    /**
     * first checks if the computer player had been attacked in the last round (the attack flag),
     * then scans its own hand for an attack card, if it finds one, it saves its index. If there is no attack card in the
     * computerplayer's hand, it does the same for a skip card
     * @return true if the computer player was attacked last turn and had either an attack card or a skip card
     */

    public boolean attackmove(){
        if(gameController.isAttackFlag()){
            for(Card card : getHand()){
                if (card instanceof AttackCard){
                    attackIndex = getHand().indexOf(card);
                    return true;
                }
                else if (card instanceof SkipCard) {
                    attackIndex = getHand().indexOf(card);
                    return true;
                }
            }
        }
        return false;
    }

    /**
     *
     * turn of the computer player. First checks if the current turn is an attack turn. If true, then it takes the attackIndex variable, returns it +1 and sets attackIndex back to 0.
     * if it is not an attack turn, it will take the size of the hand as an upper limit and generate a random integer.
     * The card at this index of the hand is then checked if it is a valid move.
     * If it is not a valid move, this process is repeated. If it is valid, the generated index+1 is returned.
     * @return
     *
     */

    public int naiveMove(){
        Random randon = new Random();
        boolean validMove = false;
        boolean isAttackTurn = attackmove();
        int attackSkipCard;
        if(isAttackTurn){
            attackSkipCard = attackIndex;
            attackIndex = 0;
            return attackSkipCard +1;
        }
        else {
            int upperBound = getHand().size()-1;
            int indexToPlay = randon.nextInt(upperBound);
            Card cardToPlay = getHand().get(indexToPlay);
            while (!validMove) {
                if (cardToPlay instanceof DefuseCard || cardToPlay instanceof NopeCard || cardToPlay instanceof FavorCard) {
                    upperBound = getHand().size()- 1;
                    indexToPlay = randon.nextInt(upperBound);
                    cardToPlay = getHand().get(indexToPlay);
                } else {
                    validMove = true;
                }
            }
            return indexToPlay +1;
        }

    }

    /**
     * needed if the human player plays a shuffle card.
     * @ensures the computer player says not to continue shuffling
     * @return false
     */
    @Override
    public boolean shouldContinueShuffling() {
        return false;
    }
}
