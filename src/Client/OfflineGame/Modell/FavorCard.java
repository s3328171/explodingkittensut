package Client.OfflineGame.Modell;

import Client.OfflineGame.Control.GameController;

import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class FavorCard extends Card {

    private GameController gameController;

    public FavorCard(GameController gameController) {

        this.gameController = gameController;
    }
    @Override
    public void activate(){

        receiveOpponentCard(gameController.getOpposingPlayer());
    }

    public void receiveOpponentCard(Player opponent) {
        // Force any other player to give you 1 card from their hand.
        // They choose which card to give you.
        int chosenIndex;
        Scanner scanner = new Scanner(System.in);
        System.out.println(opponent.toString() + ", choose a card to give to " + gameController.getCurrentPlayer().toString() + ":");
        List<Card> opponentHand = opponent.getHand();
        displayHand(opponentHand);
        if(gameController.getOpposingPlayer() instanceof ComputerPlayer){
            Random random = new Random();
            int limit = gameController.getOpposingPlayer().getHand().size();
            chosenIndex = random.nextInt(limit);
        }
        else {
            chosenIndex = scanner.nextInt();
        }
        if (chosenIndex >= 0 && chosenIndex < opponentHand.size()) {
            Card chosenCard = opponentHand.get(chosenIndex);
            gameController.getCurrentPlayer().addToHand(chosenCard);
            //this.getOwner().addToHand(chosenCard);
            opponent.removeFromHand(chosenCard);
            System.out.println(opponent.getName() + " gave " + gameController.getCurrentPlayer().toString() + " a " + chosenCard.toString());
            //System.out.println(opponent.getName() + " gave " + this.getOwner().getName() + " a " + chosenCard.getClass().getSimpleName());
        } else {
            System.out.println("Invalid index. No card given.");
        }
    }

    @Override
    public String toString(){
        return getClass().getSimpleName();
    }

    private void displayHand(List<Card> hand) {
        System.out.println("Opponent's Hand:");
        for (int i = 0; i < hand.size(); i++) {
            System.out.println(i+1 + ": " + hand.get(i).toString());
        }
    }
}
