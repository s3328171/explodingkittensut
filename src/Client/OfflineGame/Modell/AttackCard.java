package Client.OfflineGame.Modell;

import Client.OfflineGame.Control.GameController;
import Client.OfflineGame.View.GameView;

public class AttackCard extends Card{
    GameController gameController;
    Deck deck;
    GameView view;

    public AttackCard(GameController gameController, Deck deck, GameView view){
        this.gameController = gameController;
        this.deck = deck;
        this.view = view;
    }

    /**
     * sets the variables playerTurn and draw of the gamecontroller to false. This ensures that the turn of the player playing the attack card
     * ends without them drawing a card. Then it checks the attack flag of the gamecontroller. If the attack flag is true, then the opposing player gets
     * 2 turns + the remaining turns of the current player set as their remaining turns and the current player's turns get reset.
     * If the attack flag is false, the opposing player's turns get set to 2 and the current players turns get reset.
     */
    @Override
    public void activate(){
        gameController.setPlayerTurn(false);
        gameController.setDraw(false);
        if(gameController.isAttackFlag()) {
            gameController.getOpposingPlayer().setRemainingTurns(2 + gameController.getCurrentPlayer().getRemainingTurns());
            gameController.getCurrentPlayer().setRemainingTurns(0);
        }
        else{
            gameController.getOpposingPlayer().setRemainingTurns(2);
            gameController.getCurrentPlayer().setRemainingTurns(0);
        }
        /**
        try {
            if (gameController.getDeck().getActivePile().get(gameController.getDeck().getActivePile().size() - 2) instanceof AttackCard) {
                gameController.getOpposingPlayer().setRemainingTurns(2 + gameController.getCurrentPlayer().getRemainingTurns());
                //new addition
                gameController.getCurrentPlayer().setRemainingTurns(0);
            } else {
                gameController.getOpposingPlayer().setRemainingTurns(2);
                gameController.getCurrentPlayer().setRemainingTurns(0);
            }
        }
        catch (IndexOutOfBoundsException e){
            gameController.getOpposingPlayer().setRemainingTurns(2);
            gameController.getCurrentPlayer().setRemainingTurns(0);
        }
         **/
    }

    @Override
    public String toString(){
        return getClass().getSimpleName();
    }
    public void skipTurn(Player victim){
        //Skip the turn of the victim player
        //victim.playOrPass();
    }
    public void doubleTurn(Player victim, Player nextPlayer){

        //Force the victim to take 2 turns, and transfer the remaining turns to the next player
        //victim.playOrPass(); //Turn 1
        //victim.playOrPass(); //Turn 2
    }

    //Handle stacking of attacks
    public void stackAttack(Player victim, int additionalTurns) {
        //Force the victim to take additional turns on top of the previous attack
        for (int i = 0; i <additionalTurns; i++) {
        //    victim.playOrPass();
        }
    }

}


