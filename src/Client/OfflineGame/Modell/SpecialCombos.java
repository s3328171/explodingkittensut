package Client.OfflineGame.Modell;

import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class SpecialCombos extends Card{

    private String name;

    public SpecialCombos(String name){

        this.name = name;
    }

    @Override
    public String toString(){

        return name;
    }

    //Cards need to be from the same suit in order to be considered eligible for canPlayAsPair
    public static boolean canPlayAsPair(List<Card> hand) {
        // Check if there is a pair of cards with the same kind (suit) in the hand
        for (int i = 0; i < hand.size(); i++) {
            for (int j = i + 1; j < hand.size(); j++) {
                if (hand.get(i).getClass().equals(hand.get(j).getClass()) &&
                        areCardsFromSameKind(hand.get(i), hand.get(j))) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean canPlayAsTriplet(List<Card> hand) {
        // Check if there is a triplet of cards with the same kind (suit) in the hand
        for (int i = 0; i < hand.size(); i++) {
            for (int j = i + 1; j < hand.size(); j++) {
                for (int k = j + 1; k < hand.size(); k++) {
                    if (hand.get(i).getClass().equals(hand.get(j).getClass()) &&
                            hand.get(i).getClass().equals(hand.get(k).getClass()) &&
                            areCardsFromSameKind(hand.get(i), hand.get(j)) &&
                            areCardsFromSameKind(hand.get(i), hand.get(k))) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    static boolean areCardsFromSameKind(Card card1, Card card2) {
        if (card1 instanceof CatCards && card2 instanceof CatCards) {
            //If both cards are CatCards, this checks if they are of the same CatCard type
            CatCards catCard1 = (CatCards) card1;
            CatCards catCard2 = (CatCards) card2;
            return catCard1.canCombineWith(catCard2);
        } else {
            return card1.getClass().equals(card2.getClass());
        }
    }

    public static void stealRandomCard(Player currentPlayer, Player targetPlayer) {
        // Pick a random card from the target player's hand and give it to the current player
        List<Card> targetHand = targetPlayer.getHand();
        if (!targetHand.isEmpty()) {
            Random random = new Random();
            int randomIndex = random.nextInt(targetHand.size());
            Card stolenCard = targetHand.remove(randomIndex);
            currentPlayer.addToHand(stolenCard);
            System.out.println(currentPlayer.getName() + " stole a card from " + targetPlayer.getName());
        } else {
            System.out.println(targetPlayer.getName() + "'s hand is empty. No card stolen.");
        }
    }

    public static void nameAndCheckCard(Player currentPlayer, Player targetPlayer, String cardName) {
        // Check if the target player has the named card, and if so, remove it
        List<Card> targetHand = targetPlayer.getHand();
        boolean cardFound = false;

        for (Card card : targetHand) {
            if(card.toString().equals(cardName)){
                targetHand.remove(card);
                currentPlayer.getHand().add(card);
                cardFound = true;
                break;
            }
            /**
            if (card instanceof SpecialCombos) {
                SpecialCombos specialCombos = (SpecialCombos) card;
                if (specialCombos.getName().equalsIgnoreCase(cardName)) {
                    targetHand.remove(card);
                    cardFound = true;
                    break;
                }
            }
             **/
        }

        if (cardFound) {
            System.out.println(targetPlayer.getName() + " had the named card. " + currentPlayer.getName() + " successfully named and stole it.");
        } else {
            System.out.println(targetPlayer.getName() + " did not have the named card. " + currentPlayer.getName() + " is out of luck.");
        }
    }

    public static void playPair(Player currentPlayer, Player targetPlayer) {
        // Player decides to play a pair
        List<Card> currentPlayerHand = currentPlayer.getHand();

        if (canPlayAsPair(currentPlayerHand)) {
            stealRandomCard(currentPlayer, targetPlayer);
        }
    }

    public static void playTriplet(Player currentPlayer, Player targetPlayer) {
        List<Card> currentPlayerHand = currentPlayer.getHand();

        // Player decided to play a triplet
        if (canPlayAsTriplet(currentPlayerHand)) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter the name of the card you want:");
            String cardName = scanner.nextLine();
            nameAndCheckCard(currentPlayer, targetPlayer, cardName);
        }
    }

//    public static void handleSpecialComboMinusTwo(Player currentPlayer, Player targetPlayer) {
//        if (canPlayAsPair(currentPlayer.getHand())) {
//            stealRandomCard(currentPlayer, targetPlayer);
//        }
//    }
//
//    public static void handleSpecialComboMinusThree(Player currentPlayer, Player targetPlayer) {
//        if (canPlayAsTriplet(currentPlayer.getHand())) {
//            Scanner scanner = new Scanner(System.in);
//            System.out.println("Enter the name of the card you want:");
//            String cardName = scanner.nextLine();
//            nameAndCheckCard(currentPlayer, targetPlayer, cardName);
//        }
//    }

//    private String getName() {
//
//        return null;
//    }

}