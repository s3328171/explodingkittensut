package Client.OfflineGame.Modell;

import Client.OfflineGame.Control.GameController;

import java.util.Collections;
import java.util.List;

public class ShuffleCard extends Card {
    private GameController gameController;
    public ShuffleCard(GameController gameController) {
        this.gameController = gameController;
    }


    //first getDeck() gives you the object deck, the second one gives you the list called deck within the object deck
    @Override
    public void activate() {
        shuffle(gameController.getDeck().getDeck(), gameController.getOpposingPlayer());
    }
    @Override
    public String toString(){
        return getClass().getSimpleName();
    }

    // Method to shuffle the draw pile (deck list)
    public void shuffle(List<Card> drawPile, Player nextPlayer) {
        // Introduce a mechanism to control shuffling
        boolean shouldContinue = true;

        // Continue shuffling until the next player says stop
        while (shouldContinue) {
            Collections.shuffle(drawPile);

            // Assumes there's a method in Player to handle player input
            shouldContinue = nextPlayer.shouldContinueShuffling();
        }
    }
}


