package Client.OfflineGame.Modell;

import Client.OfflineGame.Control.GameController;

public class SkipCard extends Card{
    private GameController gameController;
    public SkipCard(GameController gameController){
        this.gameController = gameController;
    }

    @Override
    public void activate() {
        skipTurn();
    }

    public void skipTurn(){
        gameController.setPlayerTurn(false);
        gameController.setDraw(false);
        gameController.getCurrentPlayer().addTurns(-1);
    }
    @Override
    public String toString(){
        return getClass().getSimpleName();
    }
}
