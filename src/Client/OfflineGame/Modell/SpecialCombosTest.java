package Client.OfflineGame.Modell;

import Client.OfflineGame.Control.GameController;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class SpecialCombosTest {

    @Test
    public void testCanPlayAsPair() {
        // Test when there is a pair of cards with the same kind in the hand
        List<Card> hand = new ArrayList<>();
        hand.add(new CatCards("bikiniCat"));
        hand.add(new CatCards("bikiniCat"));
        assertTrue(SpecialCombos.canPlayAsPair(hand));

        // Test when there is no pair in the hand
        hand.clear();
        hand.add(new CatCards("bikiniCat"));
        hand.add(new SkipCard(mock(GameController.class)));
        assertFalse(SpecialCombos.canPlayAsPair(hand));

        // Add debug statements
        System.out.println("Debugging: " + SpecialCombos.canPlayAsPair(hand));
        for (Card card : hand) {
            System.out.println("Card: " + card);
        }

    }



    @Test
    public void testAreCardsFromSameKind() {
        // Test when cards are from the same kind
        CatCards catCard1 = new CatCards("tacoCat");
        CatCards catCard2 = new CatCards("tacoCat");
        assertTrue(SpecialCombos.areCardsFromSameKind(catCard1, catCard2));

        // Test when cards are not from the same kind
        CatCards catCard3 = new CatCards("bikiniCat");
        CatCards catCard4 = new CatCards("tacoCat");
        assertFalse(SpecialCombos.areCardsFromSameKind(catCard3, catCard4));
    }

    @Test
    public void testStealRandomCard() {
        // Mock players and their hands
        Player currentPlayer = mock(Player.class);
        Player targetPlayer = mock(Player.class);
        List<Card> targetHand = new ArrayList<>();
        targetHand.add(new CatCards("bikiniCat"));
        targetHand.add(new SkipCard(mock(GameController.class)));
        when(targetPlayer.getHand()).thenReturn(targetHand);

        // Perform the steal random card operation
        SpecialCombos.stealRandomCard(currentPlayer, targetPlayer);

        // Verify that a random card was stolen from the target player's hand
        verify(currentPlayer).addToHand(any(Card.class));
        assertEquals(1, targetHand.size()); // Assert that a card was removed from the target player's hand
    }

    @Test
    public void testNameAndCheckCard() {
        // Mock players and their hands
        Player currentPlayer = mock(Player.class);
        Player targetPlayer = mock(Player.class);
        List<Card> targetHand = new ArrayList<>();
        targetHand.add(new SpecialCombos("Combo1"));
        targetHand.add(new SpecialCombos("Combo2"));
        when(targetPlayer.getHand()).thenReturn(targetHand);

        // Perform the name and check card operation
        SpecialCombos.nameAndCheckCard(currentPlayer, targetPlayer, "Combo2");

        // Verify that the named card was removed from the target player's hand
        assertEquals(1, targetHand.size());
    }

    @Test
    public void testPlayPair() {
        // Mock players and their hands
        Player currentPlayer = mock(Player.class);
        Player targetPlayer = mock(Player.class);

        List<Card> currentPlayerHand = new ArrayList<>();
        currentPlayerHand.add(new SkipCard(mock(GameController.class)));
        currentPlayerHand.add(new SkipCard(mock(GameController.class)));
        when(currentPlayer.getHand()).thenReturn(currentPlayerHand);

        List<Card> targetPlayerHand = new ArrayList<>();
        targetPlayerHand.add(new DefuseCard());
        when(targetPlayer.getHand()).thenReturn(currentPlayerHand);

        // Perform the play pair operation
        SpecialCombos.playPair(currentPlayer, targetPlayer);

        //Verify that addToHand was called on currentPlayer
        verify(currentPlayer, times(1)).addToHand(any(Card.class));
    }


}
