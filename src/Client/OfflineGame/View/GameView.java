package Client.OfflineGame.View;

import Client.OfflineGame.Control.GameController;
import Client.OfflineGame.Modell.Card;
import Client.OfflineGame.Modell.ComputerPlayer;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class GameView {
    private GameController gameController;

    public GameView(GameController gameController){
        this.gameController = gameController;
    }
    public void displayWelcomeMessage() {
        System.out.println("Welcome to Exploding Kittens!");
    }


    public void initializePlayerNames(){
        System.out.println("Please enter the names of the Players");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        String[] nameSplit = name.split(" ");
        String namePlayer1 = nameSplit[0];
        String namePlayer2 = nameSplit[1];
        gameController.getDeck().getPlayer1().setName(namePlayer1);
        gameController.getDeck().getPlayer2().setName(namePlayer2);

    }

    public boolean doYouWantCom(){
        System.out.println("Do you want the second player to be a computerplayer?");
        Scanner scanner = new Scanner(System.in);
        String answer;
        do {
            answer = scanner.nextLine();
            if (!answer.equals("yes") && !answer.equals("no")) {
                System.out.println("Invalid input. Please enter 'yes' or 'no'.");
            }

        }while(!answer.equals("yes") && !answer.equals("no"));
        return answer.equals("yes");
    }


    public void displayPlayerHand(String playerName, List<Card> hand) {
        System.out.println(playerName + "'s hand: " + hand);
    }

    public void displayTurnOptions() {
        System.out.println("0: Draw a card");
        System.out.println("1 - ...: Playing a card from your hand. 1 being the most left one.");
        System.out.println("-2: playing a combo of 2 cards");
        System.out.println("-3: playing a combo of three cards");
    }

    public void defuseCardEmptyDeck(){
        System.out.println("You drew a Exploding kitten! Luckily you have a defuse card to defuse the bomb.");
        System.out.println("Oh the deck is empty! Just put it back on top of the deck!");
    }

    public void comDraw(){
        System.out.println("The computerplayer drew a card!");
    }
    public void computerPlayerTurn(Card card){
        System.out.println("The computer player played: " + card);
    }

    public void playerTurn (Card card){
        System.out.println("The player " + gameController.getCurrentPlayer().getName() + " played: "+ card);
    }


    public int defuseCard(){
        Scanner scanner = new Scanner(System.in);
        int result;
        if (gameController.getCurrentPlayer() instanceof ComputerPlayer){
            System.out.println("You drew a Exploding kitten! Luckily you have a defuse card to defuse the bomb.");
            Random random = new Random();
            int limit = gameController.getDeck().getDeck().size()-1;
            result = random.nextInt(limit);
            return result;
        }
        else {
            while (true) {
                try {
                    System.out.println("You drew a Exploding kitten! Luckily you have a defuse card to defuse the bomb.");
                    System.out.println("Where do you want to put the kitten in the deck? 1 being on top of the deck, and " + gameController.getDeck().getDeck().size() + " being the bottom.");
                    result = scanner.nextInt();
                    if (result < 1 || result > gameController.getDeck().getDeck().size()) {
                        System.out.println("Invalid input. Please enter a number between 1 and " + gameController.getDeck().getDeck().size());
                    } else {
                        break;
                    }
                } catch (InputMismatchException e) {
                    System.out.println("Invalid input. Please enter a number between 1 and " + gameController.getDeck().getDeck().size());
                    scanner.next();
                }
            }
        }

        return result;
    }

    public void printCurrentPlayer(){
        System.out.println("It is " + gameController.getCurrentPlayer() + "'s turn!");
    }

    public String nopeHasBeenFound(){
        Scanner scanner = new Scanner(System.in);
        String answer;
        if(gameController.getOpposingPlayer() instanceof ComputerPlayer){
            System.out.println("The computer player has a nope Card!");
            System.out.println("It said no!");
            return "no";
        }
        else {
            do {
                System.out.println(gameController.getOpposingPlayer().toString() + ", you have a Nope card on your hand! Do you want to play it and stop this action? (yes/no)");
                answer = scanner.nextLine();
                if (!answer.equals("yes") && !answer.equals("no")) {
                    System.out.println("Invalid input. Please enter 'yes' or 'no'.");
                }
            } while (!answer.equals("yes") && !answer.equals("no"));
        }
        return answer.toLowerCase();
    }

    public void noped(){
        System.out.println("NOPE");
    }
    public String yupHasBeenFound(){
        Scanner scanner = new Scanner(System.in);
        String answer;
        if(gameController.getCurrentPlayer() instanceof ComputerPlayer){
            System.out.println("The computer player also has a nope Card!");
            System.out.println("It said no!");
            return "no";
        }
        do {
            System.out.println(gameController.getOpposingPlayer().toString() + ", you have a Nope card on your hand! Do you want to play it and stop this action? (yes/no)");
            answer = scanner.nextLine();
            if(!answer.equals("yes") && !answer.equals("no")){
                System.out.println("Invalid input. Please enter 'yes' or 'no'.");
            }
        }while(!answer.equals("yes") && !answer.equals("no"));
        return answer.toLowerCase();
    }

    public void yup(){
        System.out.println("YUP");
    }

    public void invalidCard(){
        System.out.println("Oops that is not a valid card to play! Try again!");
    }
    public void attackCardMessage(){
        System.out.println(gameController.getCurrentPlayer().toString() + ", you have been attacked! it is now your turn \n" +
                "and you have to go " + gameController.getCurrentPlayer().getRemainingTurns() + "times!");
    }

    public int getPlayerChoice() {
        System.out.print("Choose an option: ");
        Scanner scanner = new Scanner(System.in);
        int answer;
        while(true) {
            try {
                answer = scanner.nextInt();
                if (answer > gameController.getCurrentPlayer().getHand().size()) {
                    System.out.println("Invalid input. Please enter a number between 0 and " + gameController.getCurrentPlayer().getHand().size());
                }
                else{
                    break;
                }
            } catch (InputMismatchException e) {
                System.out.println("Invalid input. Please enter a number between 0 and " + gameController.getCurrentPlayer().getHand().size());
                scanner.next();
            }
        }
        return answer;
    }

    public int getPlayerComboChoice(int i){
        System.out.print("Choose " + i + " combo card:");
        Scanner scanner = new Scanner(System.in);
        int answer;
        while(true) {
            try {
                answer = scanner.nextInt();
                if (answer < 0 || answer > gameController.getCurrentPlayer().getHand().size()) {
                    System.out.println("Invalid input. Please enter a number between 0 and " + gameController.getCurrentPlayer().getHand().size());
                }
                else{
                    break;
                }
            } catch (InputMismatchException e) {
                System.out.println("Invalid input. Please enter a number between 0 and " + gameController.getCurrentPlayer().getHand().size());
                scanner.next();
            }
        }
        return answer;
    }


    public void displayExplodedMessage() {
        System.out.println("BOOM! You drew an Exploding Kitten! Game Over!");
    }
}